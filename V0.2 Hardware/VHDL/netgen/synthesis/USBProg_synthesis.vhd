--------------------------------------------------------------------------------
-- Copyright (c) 1995-2011 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: O.40d
--  \   \         Application: netgen
--  /   /         Filename: USBProg_synthesis.vhd
-- /___/   /\     Timestamp: Tue Apr 10 14:58:36 2012
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm USBProg -w -dir netgen/synthesis -ofmt vhdl -sim USBProg.ngc USBProg_synthesis.vhd 
-- Device	: xc3s200a-4-vq100
-- Input file	: USBProg.ngc
-- Output file	: C:\Work\BLL\USBProg\PLD\USBProg\netgen\synthesis\USBProg_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: USBProg
-- Xilinx	: C:\Xilinx\13.1\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity USBProg is
  port (
    pMCLR : out STD_LOGIC; 
    pnRXRDY : in STD_LOGIC := 'X'; 
    pWR : out STD_LOGIC; 
    pnRD : out STD_LOGIC; 
    pSI_WU : out STD_LOGIC; 
    pnTXRDY : in STD_LOGIC := 'X'; 
    pX : in STD_LOGIC := 'X'; 
    pnCSV1 : out STD_LOGIC; 
    pnCSV2 : out STD_LOGIC; 
    pnINIT : out STD_LOGIC; 
    pSCK : out STD_LOGIC; 
    pSDI : in STD_LOGIC := 'X'; 
    pSDO : out STD_LOGIC; 
    paTOE20 : out STD_LOGIC; 
    paTOE22 : out STD_LOGIC; 
    paTOE18 : out STD_LOGIC; 
    paTOE19 : out STD_LOGIC; 
    pCLK : in STD_LOGIC := 'X'; 
    pnCSADC : out STD_LOGIC; 
    p15V_EN : out STD_LOGIC; 
    pnCSWREG : out STD_LOGIC; 
    paTIO : inout STD_LOGIC_VECTOR ( 21 downto 0 ); 
    paD : inout STD_LOGIC_VECTOR ( 7 downto 0 ); 
    paTOE : out STD_LOGIC_VECTOR ( 11 downto 0 ); 
    paTI : in STD_LOGIC_VECTOR ( 3 downto 0 ) 
  );
end USBProg;

architecture Structure of USBProg is
  signal N0 : STD_LOGIC; 
  signal N1 : STD_LOGIC; 
  signal N2 : STD_LOGIC; 
  signal N3 : STD_LOGIC; 
  signal N4 : STD_LOGIC; 
  signal N5 : STD_LOGIC; 
  signal N6 : STD_LOGIC; 
  signal N7 : STD_LOGIC; 
  signal paTOE_9_inv : STD_LOGIC; 
  signal paTOE_8_OBUF_62 : STD_LOGIC; 
  signal paTOE_8_OBUF1 : STD_LOGIC; 
  signal paTOE_9_OBUF_64 : STD_LOGIC; 
  signal pnRD_OBUF_71 : STD_LOGIC; 
  signal pnRXRDY_IBUF_73 : STD_LOGIC; 
  signal pnRXRDY_inv : STD_LOGIC; 
  signal sRD_75 : STD_LOGIC; 
  signal saTOE : STD_LOGIC_VECTOR ( 7 downto 0 ); 
begin
  XST_GND : GND
    port map (
      G => paTOE_9_OBUF_64
    );
  XST_VCC : VCC
    port map (
      P => paTOE_9_inv
    );
  sRD : FDR
    port map (
      C => paTOE_8_OBUF_62,
      D => paTOE_9_inv,
      R => pnRXRDY_IBUF_73,
      Q => sRD_75
    );
  saTOE_2 : FDE
    port map (
      C => paTOE_8_OBUF_62,
      CE => pnRXRDY_inv,
      D => N5,
      Q => saTOE(2)
    );
  saTOE_0 : FDE
    port map (
      C => paTOE_8_OBUF_62,
      CE => pnRXRDY_inv,
      D => N7,
      Q => saTOE(0)
    );
  saTOE_1 : FDE
    port map (
      C => paTOE_8_OBUF_62,
      CE => pnRXRDY_inv,
      D => N6,
      Q => saTOE(1)
    );
  saTOE_3 : FDE
    port map (
      C => paTOE_8_OBUF_62,
      CE => pnRXRDY_inv,
      D => N4,
      Q => saTOE(3)
    );
  saTOE_4 : FDE
    port map (
      C => paTOE_8_OBUF_62,
      CE => pnRXRDY_inv,
      D => N3,
      Q => saTOE(4)
    );
  saTOE_7 : FDE
    port map (
      C => paTOE_8_OBUF_62,
      CE => pnRXRDY_inv,
      D => N0,
      Q => saTOE(7)
    );
  saTOE_5 : FDE
    port map (
      C => paTOE_8_OBUF_62,
      CE => pnRXRDY_inv,
      D => N2,
      Q => saTOE(5)
    );
  saTOE_6 : FDE
    port map (
      C => paTOE_8_OBUF_62,
      CE => pnRXRDY_inv,
      D => N1,
      Q => saTOE(6)
    );
  pCLK_IBUF : IBUF
    port map (
      I => pCLK,
      O => paTOE_8_OBUF1
    );
  pnRXRDY_IBUF : IBUF
    port map (
      I => pnRXRDY,
      O => pnRXRDY_IBUF_73
    );
  paTIO_21_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(21)
    );
  paTIO_20_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(20)
    );
  paTIO_19_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(19)
    );
  paTIO_18_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(18)
    );
  paTIO_17_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(17)
    );
  paTIO_16_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(16)
    );
  paTIO_15_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(15)
    );
  paTIO_14_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(14)
    );
  paTIO_13_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(13)
    );
  paTIO_12_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(12)
    );
  paTIO_11_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(11)
    );
  paTIO_10_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(10)
    );
  paTIO_9_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(9)
    );
  paTIO_8_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(8)
    );
  paTIO_7_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(7)
    );
  paTIO_6_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(6)
    );
  paTIO_5_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(5)
    );
  paTIO_4_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(4)
    );
  paTIO_3_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(3)
    );
  paTIO_2_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(2)
    );
  paTIO_1_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(1)
    );
  paTIO_0_OBUFT : OBUFT
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => paTIO(0)
    );
  paD_7_IOBUF : IOBUF
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => N0,
      IO => paD(7)
    );
  paD_6_IOBUF : IOBUF
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => N1,
      IO => paD(6)
    );
  paD_5_IOBUF : IOBUF
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => N2,
      IO => paD(5)
    );
  paD_4_IOBUF : IOBUF
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => N3,
      IO => paD(4)
    );
  paD_3_IOBUF : IOBUF
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => N4,
      IO => paD(3)
    );
  paD_2_IOBUF : IOBUF
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => N5,
      IO => paD(2)
    );
  paD_1_IOBUF : IOBUF
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => N6,
      IO => paD(1)
    );
  paD_0_IOBUF : IOBUF
    port map (
      I => paTOE_9_OBUF_64,
      T => paTOE_9_inv,
      O => N7,
      IO => paD(0)
    );
  pMCLR_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => pMCLR
    );
  pWR_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => pWR
    );
  pnRD_OBUF : OBUF
    port map (
      I => pnRD_OBUF_71,
      O => pnRD
    );
  pSI_WU_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => pSI_WU
    );
  pnCSV1_OBUF : OBUF
    port map (
      I => paTOE_9_inv,
      O => pnCSV1
    );
  pnCSV2_OBUF : OBUF
    port map (
      I => paTOE_9_inv,
      O => pnCSV2
    );
  pnINIT_OBUF : OBUF
    port map (
      I => paTOE_9_inv,
      O => pnINIT
    );
  pSCK_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => pSCK
    );
  pSDO_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => pSDO
    );
  paTOE20_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => paTOE20
    );
  paTOE22_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => paTOE22
    );
  paTOE18_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => paTOE18
    );
  paTOE19_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => paTOE19
    );
  pnCSADC_OBUF : OBUF
    port map (
      I => paTOE_9_inv,
      O => pnCSADC
    );
  p15V_EN_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => p15V_EN
    );
  pnCSWREG_OBUF : OBUF
    port map (
      I => paTOE_9_inv,
      O => pnCSWREG
    );
  paTOE_11_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => paTOE(11)
    );
  paTOE_10_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => paTOE(10)
    );
  paTOE_9_OBUF : OBUF
    port map (
      I => paTOE_9_OBUF_64,
      O => paTOE(9)
    );
  paTOE_8_OBUF : OBUF
    port map (
      I => paTOE_8_OBUF1,
      O => paTOE(8)
    );
  paTOE_7_OBUF : OBUF
    port map (
      I => saTOE(7),
      O => paTOE(7)
    );
  paTOE_6_OBUF : OBUF
    port map (
      I => saTOE(6),
      O => paTOE(6)
    );
  paTOE_5_OBUF : OBUF
    port map (
      I => saTOE(5),
      O => paTOE(5)
    );
  paTOE_4_OBUF : OBUF
    port map (
      I => saTOE(4),
      O => paTOE(4)
    );
  paTOE_3_OBUF : OBUF
    port map (
      I => saTOE(3),
      O => paTOE(3)
    );
  paTOE_2_OBUF : OBUF
    port map (
      I => saTOE(2),
      O => paTOE(2)
    );
  paTOE_1_OBUF : OBUF
    port map (
      I => saTOE(1),
      O => paTOE(1)
    );
  paTOE_0_OBUF : OBUF
    port map (
      I => saTOE(0),
      O => paTOE(0)
    );
  paTOE_8_OBUF_BUFG : BUFG
    port map (
      I => paTOE_8_OBUF1,
      O => paTOE_8_OBUF_62
    );
  pnRXRDY_inv1_INV_0 : INV
    port map (
      I => pnRXRDY_IBUF_73,
      O => pnRXRDY_inv
    );
  pnRD1_INV_0 : INV
    port map (
      I => sRD_75,
      O => pnRD_OBUF_71
    );

end Structure;

