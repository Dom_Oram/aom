#include <string>
#include <windows.h>
#include "ftd2xx.h"
#include <vector>
using namespace std;

#ifndef DDS_H
#define DDS_H
	FT_STATUS initDDS(FT_HANDLE ftHandle1, float clockSpeed);
	FT_STATUS readDDSReg(char regNum, size_t dataLength);
	FT_STATUS writeDDSRegHoldWrite(char regNum, size_t dataLength, char* data);
	FT_STATUS writeTrigger(char prof, size_t dataLength, char RegNum, char* data);
	void setDDSDebug(bool print);
	void closeDDS();
	FT_STATUS readProfileReg(int regNum);
	FT_STATUS writeFileToRAM(string filename, int end, float FTW, bool isAM);
	FT_STATUS readRAM();
	FT_STATUS toggleRamp();
	FT_STATUS writeSweep(float startCF, float endCF, float startDev, float endDev, float startMod, float overallTime, float endPow);
	FT_STATUS createRamp(float endFreq, float startFreq, char* stepSize, char* rampRate, bool intoTrigger = false);
	FT_STATUS createCW(int profile, float amp, float freq);
	FT_STATUS goToProfile(int prof);
	FT_STATUS toggleTrigger();
	FT_STATUS createTriangleFM(float centre, float dev, float mod, float power);
#endif