#include "DDS.h"
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

static FT_HANDLE ftHandle;

static FT_STATUS ftStatus = 0;

static unsigned char ReadBuffer[65535];
static unsigned char WriteBuffer[65535];

static unsigned char Data[65535];

static DWORD NumBytesToSend = 0;

static DWORD NumBytesRead = 0;

static int NumDataBytes = 0;

static bool printDebug = 0;

#define FPGA_SPEED 12

static float sys_clock = 0;

#define RAM_LENGTH 1024
#define TIMEOUT 100000
#define MAX_ASF 16383

//Commands
#define READ_DDS		0x20
#define RAM_RESET		0x26
#define READ_RAM		0x27
#define READ_TRIG		0x29
#define TRIG_WAIT		0x2A
#define WRITE_TRIG		0x40
#define WRITE_SWEEP		0x50
#define TOGGLE_SWEEP	0x2D

//HELPER FUNCTIONS

vector<unsigned char> intToCharStream(int toConvert)
{
	///
	///Helper function to convert an integer into a binary byte in the form of a char
	///

    vector<unsigned char> output (4);
	for (int i = 0; i < 4; i++)
		output[3-i] = (toConvert >> (i * 8));
	return output;
}

vector<unsigned char> intToCharStream(int toConvert, int offset)
{
	///
	///Overloaded helper function to introduce an offset when converting integers to binary
	///

    vector<unsigned char> output (5);
	vector<unsigned char> input (4);
	
	input = intToCharStream(toConvert);
	
	output[0] = input[0] >> (8-offset);

	for (int i = 1; i < 4; i++)
		output[i] = (input[i] >> (8-offset)) | (input[i-1] << offset);
	
	output[4] = input[3] << offset;

	return output;
}

int calcFTW (float frequency)
{
	///
	///Returns the frequency tuning word that, when given to the DDS, will produce the desired frequency
	///

	int answer = floor(pow(2.0,32)*frequency/sys_clock + 0.5);
	return answer;
}

static void copyDataToBuffer()
{
	///
	/// Copies data so that it is placed in the command RAM of the FPGA
	///

	//seperate data into LSN and MSN to load register for sending
	for (int i = 0; i < NumDataBytes; i++)
	{
		//send LSN
		WriteBuffer[NumBytesToSend++] = (0x0F & Data[i]);

		//send MSN
		WriteBuffer[NumBytesToSend++] = ((Data[i] >> 4) + 16);
	}

	NumDataBytes = 0;
}


static FT_STATUS writeChip()
{
	///
	/// Writes the contents of WriteBuffer[] to the FPGA
	///

	DWORD BytesWritten = 0;

	if (printDebug)
	{
		cout << "Writing: ";
		for (int i=0; i <NumBytesToSend; i++)
		{
			cout << hex << (int) WriteBuffer[i] << " ";
		}
		cout << "\n\n";
	}

	ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX);
	ftStatus |= FT_Write(ftHandle, &WriteBuffer, NumBytesToSend, &BytesWritten);

	NumBytesToSend = 0;

	return ftStatus;
}


//MAIN FUNCTIONS

FT_STATUS writeDDSReg(char regNum, size_t dataLength, char* data, bool keepInBuffer = false)
{
	///
	///Write data to a given DDS register
	///

	ftStatus = 0;
	//NumDataBytes = 0;
	
	//check inputs
	if ((dataLength > 8))
	{
		cout << "Error, must send less than 8 bytes to a register if not RAM\n";
		return 1;
	}
	if (regNum > 22)
	{
		cout << "Error, only 22 registers on the AD9910\n";
		return 1;
	}

	//write instruction
	char instruction = 0x00 | (((dataLength - 2) >> 1) << 5) | regNum;

	if (dataLength == 0)
	{
		instruction = 0x00 | regNum;
	}

	Data[NumDataBytes++] = instruction;

	//write data (RAM is backwards)
	if (dataLength != 0)
	{
		for (int i=0; i<(int)dataLength; i++)
		{
			Data[NumDataBytes++] = data[i];
		}
	}else{
		for (int i=RAM_LENGTH*4-1; i >= 0; i--)
		{
			Data[NumDataBytes++] = data[i];
		}
	}

	if (keepInBuffer == false)
	{
		copyDataToBuffer();
		writeChip();
	}

	//if (printDebug == true)
	//{
	//	cout << "\nDDS Register Write\n";

	//	cout << "DataLength: " << hex << (int)(((dataLength - 2) >> 1) << 5);

	//	cout << "\nInstruction " << hex << (int)instruction << "\n";
	//	cout << "\n";
	//}

	return ftStatus;
}


static DWORD readChip(bool print)
{
	///
	///Read bytes back from the FPGA
	///

	DWORD NumBytesToRead = 0; // Number of bytes available to read in the driver's input buffer
	int time = 0;
	
	//attempt to get bytes until timeout
	do
	{
		// Get the number of bytes in the device input buffer
		ftStatus = FT_GetQueueStatus(ftHandle, &NumBytesToRead);
		time ++;
		if (time > TIMEOUT)
		{
			return 0;
		}
	} while ((NumBytesToRead == 0) && (ftStatus == FT_OK));

	//print read
	ftStatus = FT_Read(ftHandle, &ReadBuffer, NumBytesToRead, &NumBytesRead); 
	if(print){
		for (unsigned int i = 0; i < NumBytesToRead; i++)
			cout << hex << (int)ReadBuffer[i] << " ";
		cout << "\n";
	}

	return NumBytesRead;
}

FT_STATUS initDDS(FT_HANDLE ftHandle1, float clockSpeed)
{
	///
	///Initialises the DDS with the reference clock and the PLL multiplier. This increases the multiplier until the reference is at a maximum.
	///

	//set the handle of the device
	ftHandle = ftHandle1;

	//calculate max sys clock
	float	temp_sys	= 0;
	int		i			= 0;
	int		multiplier  = 0;
	do
	{		
		
		sys_clock	= temp_sys;
		multiplier	= i;

		i++;
		temp_sys = i*clockSpeed;

	}
	while(temp_sys <= 1e9);

	cout << "Sys_clock " << sys_clock << "\n";
	cout << "Multiplier " << dec << multiplier << "\n";

	char multiplier_byte = (multiplier << 1);

	printDebug = 1;

	//set PLL on the DDS
	//VCO = VCO5 = b101
	//I_CP = 387 = b111
	//DRV0 = default = b01
	//REFCLK divider = 0
	//REFCLK reset = 1
	//PFD reset = 0
	//PLL enable = 1
	//N = multiplier
	char PLL[] = {0x1D, 0x3F, 0x41, 0x00};
	PLL[3] = multiplier_byte;

	writeDDSReg(0x02, 4, PLL);

	return 0;

}

FT_STATUS createCW(int profile, float amp, float freq)
{
	///
	///Creates a single tone on the given DDS profile
	///

	//convert profile
	unsigned char prof = (char)(profile + 14);
	cout << "Single Tone on prof: " << hex << (int)prof << "\n";

	//set amp settings
	char settings[] = {0x01, 0x00, 0x00, 0x00};
	//writeDDSReg(0x01, 4, settings);

	char settings0[] = {0x00, 0x40, 0x00, 0x00};
	//writeDDSReg(0x00, 4, settings);

	//create data to send
	char data[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

	vector<unsigned char> FTW = intToCharStream(calcFTW(freq));
	vector<unsigned char> ASF = intToCharStream(amp*MAX_ASF);

	for (int i=0; i<4; i++)
	{
		data[4+i]	= FTW[i];
	}
	
	data[0]		= ASF[2];
	data[1]		= ASF[3];

	cout << "ASF: " << amp*MAX_ASF << "\n";

	if ((amp > 16383) || (amp < 0))
	{
		cout << "Amplitude of single wave out of range!!\n";
		return 1;
	}else{	
		writeDDSReg(prof, 8, data);
		return ftStatus;
	}

}

FT_STATUS goToProfile(int prof)
{
	///
	///Sets the profile on the DDS
	///

	//convert into to byte (0x3X)
	char instruction = 0x30 | ((char)prof & 0x0F);

	WriteBuffer[NumBytesToSend++] = instruction;

	return writeChip();
}


FT_STATUS readDDSReg(char regNum, size_t dataLength)
{
	///
	///Instructs the FPGA to read a given DDS register and reads this information back.
	///There is a known issue here in that the FPGA will always return 8 bytes despite some registers being shorter
	///

	ftStatus = 0;
	NumDataBytes = 0;

	//write instruction
	char instruction = 0x80 | (((dataLength - 2) >> 1) << 5) | regNum;

	Data[NumDataBytes++] = instruction;

	//write data
	for (int i=0; i<(int)dataLength; i++)
	{
		Data[NumDataBytes++] = 0x00;
	}

	copyDataToBuffer();
	
	writeChip();

	readChip(false);
	
	WriteBuffer[NumBytesToSend++] = READ_DDS; //get read buffer

	writeChip();

	//read data
	if (readChip(true) == 0)
	{
		ftStatus = 1; //error
	}

	return ftStatus;
}

FT_STATUS writeTrigger(char prof, size_t dataLength, char regNum, char* data)
{
	///
	///Writes data into the trigger RAM on the FPGA, which is sent to the DDS device when a trigger signal is recieved by the FPGA.
	///

	//write instruction
	char command = WRITE_TRIG | ((char)prof & 0x0F);
	WriteBuffer[NumBytesToSend++] = command;
	
	//write amount of data
	WriteBuffer[NumBytesToSend++] = (char)dataLength;

	//write register
	char instruction = 0x00 | (((dataLength - 2) >> 1) << 5) | regNum;

	WriteBuffer[NumBytesToSend++] = instruction;

	//write data
	for (int i=0; i<(int)dataLength; i++)
	{
		WriteBuffer[NumBytesToSend++] = data[i];
	}

	writeChip();

	Sleep(1000);

	readChip(true);

	//read Trigger
	WriteBuffer[NumBytesToSend++] = READ_TRIG;

	writeChip();

	cout << "Trigger RAM Read: ";

	if (readChip(true) == 0)
	{
		cout << "No Bytes Recieved\n";
	}

	return ftStatus;
}

FT_STATUS toggleTrigger()
{
	///
	///Set FPGA to wait for trigger
	///

	WriteBuffer[NumBytesToSend++] = TRIG_WAIT;

	return writeChip();
}

FT_STATUS readProfileReg(int regNum)
{
	///
	///Sets profile pins correctly to allow the profile to be read
	///

	ftStatus = 0;
	NumDataBytes = 0;

	//set profile pins correctly
	char profile = 0x30 | regNum;
	cout << "\nProfile: " << hex << (int)profile; 
	WriteBuffer[NumBytesToSend++] = profile; //set profile

	char regHex = regNum + 14;
	cout << "\nReg: " << hex << (int)regHex;
	readDDSReg(regHex, 8);

	return ftStatus;
}

FT_STATUS writeFileToRAM(string filename, int end, float FTW, bool isAM)
{
	///
	///Write the contents of a hex file into DDS RAM via the FPGA
	///

	//set RAM to disable (INEFFECIENT)
	/*
	readDDSReg(0x00, 4);

	cout << NumBytesRead << "\n\n";
	
	if (ReadBuffer[3] && 0x80 == 0x80)
	{
		char byte = ReadBuffer[3] | 0x00;
		writeDDSReg(0x00, 4, {ReadBuffer[0],
	}
	*/

	//open file
	ifstream RAM_FILE;
	RAM_FILE.open (filename.c_str(), ios::binary);
	ftStatus |= RAM_FILE.fail();

	//set profile to 0 covering all RAM
	char profLoad[] = {0x00, 0x00, 0x00, 0xFF, 0xC0, 0x00, 0x00, 0x02};
	writeDDSReg(0x0E, 8, profLoad);
	WriteBuffer[NumBytesToSend++] = 0x30; //set profile 0

	//program RAM
	char RAM[RAM_LENGTH*4];
	cout << "Reading: ";
	for (int i = 0; i < (RAM_LENGTH*4); i++)
	{
		//cout << hex << (int)RAM_FILE.peek() << " ";
		
		RAM[i] = (int)RAM_FILE.get();

	}

	//turn off print so my eyes don't hurt
	printDebug = 0;
	writeDDSReg(0x16, 0, RAM);
	printDebug = 1;

	//set profile 0 to required range
	vector<unsigned char> endBytes = intToCharStream(end, 6);
	
	//cout << "End Address: ";
	char prof0[] = {0x00, 0x00, 0x60, 0x00, 0xC0, 0x00, 0x00, 0x03};

	writeDDSReg(0x0E, 8, prof0);

	WriteBuffer[NumBytesToSend++] = 0x30; //set profile 0
	
	if (isAM)
	{
		//set FTW
		writeDDSReg(0x07, 4, reinterpret_cast<char*> (&intToCharStream(calcFTW(FTW))[0]));

		//set RAM up (ATW/FTW, enable)
		char RAMType[] = {0xC0, 0x40, 0x00, 0x00};
		writeDDSReg(0x00, 4, RAMType);
	}else{
		//set ASF
		char ASF [] = {0x00, 0x00, 0x01, 0xB0};
		writeDDSReg(0x09, 4, ASF);

		//set RAM up (ATW/FTW, enable)
		char RAMType[] = {0x80, 0xC0, 0x02, 0x00};
		writeDDSReg(0x00, 4, RAMType);
	}

	return ftStatus;
}

FT_STATUS readRAM()
{
	///
	/// Read back the DDS RAM
	///

	char end_lower_byte		= 0x40;
	char end_upper_byte		= 0x00;
	char start_upper_byte	= 0x00;
	char start_lower_byte	= 0x00;

	//set profile to 1 covering 2 RAM locations
	char prof[] = {0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00};

	WriteBuffer[NumBytesToSend++] = 0x31; //set profile 1

	for (int i=0; i<50; i++)
	{
		cout << "Writing: ";
		for (int j = 0; j < 8; j++)
		{
			cout << hex << (int)prof[j];
		}
		cout << "\n";

		writeDDSReg(0x0F, 8, prof);
		
		writeChip();

		while (readDDSReg(0x16, 8) != 0)
		{
			cout << "Error in read, retrying\n";
			writeDDSReg(0x0F, 8, prof);
		
			writeChip();
		}

		end_lower_byte		+= 128;
		start_lower_byte	+= 128;
		if (end_lower_byte == 0x40)
		{
			end_upper_byte += 1;
		}
		if (start_lower_byte == 0x00)
		{
			start_upper_byte += 1;
		}
		prof[6] = start_lower_byte;
		prof[5] = start_upper_byte;

		prof[4] = end_lower_byte;
		prof[3] = end_upper_byte;

	}
	WriteBuffer[NumBytesToSend++] = 0x30; //set profile 0

	return ftStatus;
	
}

FT_STATUS createTriangleFM (float centre, float dev, float mod, float power)
{
	///
	///Creates a singular triangle modulated waveform using the DDS linear ramp function
	///

	int		rate = 1;
	float	step = 4*pow(2.0,32)*mod*dev*4/(pow(sys_clock,2));
	cout << "STEP " << dec << step << "\n";

	if (ceil(step) != (int)step)
	{
		step += 0.5;
	}

	//set continuous linear ramp
	char settings []= {0x00, 0x0E, 0x00, 0x00};
	writeDDSReg(0x01, 4, settings);

	//set amplitude settings
	char ATW_setup[] = {0x00, 0x80, 0x02, 0x00};
	writeDDSReg(0x00, 4, ATW_setup);

	//set amplitude
	writeDDSReg(0x09, 4, reinterpret_cast<char*> (&intToCharStream(power)[0]));

	char rampRate[]		= {0x00, 0x01};

	createRamp(centre-dev, centre+dev, reinterpret_cast<char*> (&intToCharStream(step)[0]), rampRate);	

	return ftStatus;
}

FT_STATUS writeSweep(float startCF, float endCF, float startDev, float endDev, float startMod, float overallTime, float endPow)
{
	///
	///Sends commands to the FPGA to create a sweep between two triangular modulated waveforms.
	///Each step change in the sweep is sent seperately and stored in the FPGA RAM.
	///

	int		RAM_length		= 1023;
	int		bytesPerRamp	= 14;
	int		num_of_steps	= floor((float)RAM_length/(float)bytesPerRamp);
	float	mod_ind			= startDev/startMod;

	cout << "Num of Steps: " << dec << num_of_steps << "\n";

	//turn on continuous ramping
	char settings []= {0x00, 0x0E, 0x00, 0x00};
	writeDDSReg(0x01, 4, settings);

	//char ASF[] = {0x00, 0x00, 0xFF, 0xFC}; //454 mV
	//writeDDSReg(0x09, 4, ASF);

	//set amplitude settings
	char ATW_setup[] = {0x00, 0x80, 0x02, 0x00};
	writeDDSReg(0x00, 4, ATW_setup);
	
	char	rampRate[]	= {0x00, 0x01, 0x00, 0x01};
	writeDDSReg(0x0D, 4, rampRate);
	
	float	step		= 4*pow(2.0,32)*startMod*startDev*4/(pow(sys_clock,2));

	//set up sweep write.
	WriteBuffer[NumBytesToSend++] = WRITE_SWEEP;

	//first give time for each update
	float clocks_per_up = overallTime*12e6/num_of_steps;
	cout << "Clocks Per Update: " << dec << clocks_per_up << "\n";
	if (clocks_per_up > 65535)
	{
		cout << "Error, time between writes too long!! Reduce overall ramp time. \n";
	}

	if (overallTime/num_of_steps < 60e-6) //rough (depends on the speed of the FPGA)
	{
		cout << "Error, time between writes too short!! Increase overall ramp time. \n";
	}

	cout << "Time per update: " << dec << overallTime/num_of_steps << "\n";

	//send wait time (two bytes LSB first)
	vector<unsigned char> wait = intToCharStream(clocks_per_up);

	WriteBuffer[NumBytesToSend++] = wait[3];
	WriteBuffer[NumBytesToSend++] = wait[2];

	//send length (two bytes LSB first)
	vector<unsigned char> length = intToCharStream(num_of_steps*bytesPerRamp+1);
	
	WriteBuffer[NumBytesToSend++] = length[3];
	WriteBuffer[NumBytesToSend++] = length[2];

	num_of_steps--; //include final 'reset' step

	//create waveform
	float	central		= startCF;
	float	dev			= startDev;
	float	mod			= startMod;
	float	startPow	= 16383*4;
	float	power		= startPow;
	for (int i = 0; i < num_of_steps; i++)
	{
		central -= (startCF-endCF)/num_of_steps;
		//dev		-= (startDev-endDev)/num_of_steps;
		//mod		= dev/mod_ind;
		//step	= 4*pow(2.0,32)*mod*dev*4/(pow(sys_clock,2));
		power		-= (startPow-endPow)/num_of_steps;

		//cout << "Central: " << dec << central << "\n";
		//cout << "Dev: " << dec << dev << "\n";
		//cout << "Mod: " << dec << mod << "\n";
		//cout << "Pow: " << dec << power << "\n";
		
		writeDDSReg(0x09, 4, reinterpret_cast<char*> (&intToCharStream(power)[0]), true);
		
		createRamp(central-dev, central+dev, reinterpret_cast<char*> (&intToCharStream(step)[0]), rampRate, true);		

		//write buffer straight into FPGA rather than copy into command RAM
		for (int i = 0; i < NumDataBytes; i++)
		{
			//send byte
			WriteBuffer[NumBytesToSend++] = Data[i];
		}
	
		NumDataBytes = 0;
		writeChip();
	}

	
	//final step
	writeDDSReg(0x09, 4, reinterpret_cast<char*> (&intToCharStream(startPow)[0]), true);
	
	createRamp(central-dev, central+dev, reinterpret_cast<char*> (&intToCharStream(4*pow(2.0,32)*startMod*startDev*4/(pow(sys_clock,2)))), rampRate, true);		

	//write buffer straight into FPGA rather than copy into command RAM
	for (int i = 0; i < NumDataBytes; i++)
	{
		//send byte
		WriteBuffer[NumBytesToSend++] = Data[i];
	}	
	
	//turn on sweep
	WriteBuffer[NumBytesToSend++] = TOGGLE_SWEEP;
	writeChip();


	return ftStatus;
}

FT_STATUS toggleRamp()
{

	WriteBuffer[NumBytesToSend++] = TOGGLE_SWEEP;
	return writeChip();
}


FT_STATUS createRamp(float startFreq, float endFreq,  char* stepSize, char* rampRate, bool intoTrigger)
{
	///
	///Produces a linearly ramping frequency using the Digital Ramp Generator on the DDS
	///

	/*
	char ATW_setup[] = {0x00, 0x80, 0x02, 0x00};
	writeDDSReg(0x00, 4, ATW_setup);

	char ASF[] = {0x00, 0x00, 0x03, 0x00}; //454 mV
	writeDDSReg(0x09, 4, ASF);

	char settings1 []= {0x00, 0x0E, 0x00, 0x00};
	writeDDSReg(0x01, 4, settings1);
	*/

	vector<unsigned char> lowerLim = intToCharStream(calcFTW(startFreq));
	vector<unsigned char> upperLim = intToCharStream(calcFTW(endFreq));

	char size[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	char rate[] = {0x00, 0x00, 0x00, 0x00};
	char limits[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

	for (int i = 0; i < 4; i++)
	{
		limits[i]	=	upperLim[i];
		limits[i+4]	=	lowerLim[i];
		size[i]		=	stepSize[i];
		size[i+4]	=	stepSize[i];
	}
	for (int i = 0; i <2; i++)
	{
		rate[i]		=	rampRate[i];
		rate[i+2]	=	rampRate[i];
	}

	writeDDSReg(0x0B, 8, limits, intoTrigger);
	
	if (intoTrigger == false)
	{
		writeDDSReg(0x0D, 4, rate, intoTrigger);

		writeDDSReg(0x0C, 8, size, intoTrigger);
	}
	return ftStatus;
}

void setDDSDebug(bool print)
{
	printDebug = print;
}

void closeDDS()
{
	FT_Close(ftHandle);
}

