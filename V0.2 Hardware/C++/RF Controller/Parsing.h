#include <string>
#ifndef PARSE_H
#define PARSE_H
	static string getNextWord(string line, size_t start)
	{ 
		//get the next word in line, starting at character number start.
		string word;
		
		//cut before beginning of word
		if ( line.find_first_of(" ") != -1) word.append(line.substr(start, line.length()));
		else{
			cout << "Parsing error\n";
		}

		//cut after the end of word
		if ( word.find_first_of(" ") != -1) word = word.substr(0, word.find_first_of(" "));
		//else final word in line

		return word;
	};
	static string getBracketData(string line, size_t start)
	{
		//get the next bracketted word in line, starting at character number start.
		string data;

		if ( line.find_first_of("(") != -1) data.append(line.substr(line.find_first_of("(") + 1, line.length()));
		else cout << "Parsing error";

		if ( data.find_first_of(")") != -1) data = data.substr(0, data.find_first_of(")"));
		else cout << "Parsing error";

		return data;
	};
#endif
