
\documentclass[onecolumn,10.5pt]{revtex4}

\usepackage{graphics,graphicx,epsfig,ulem} 

\begin{document}

\textheight=24.75cm

\title{Technical Document for RF Generator} 

\maketitle

\section{Overview of Device Layout}

The device contains three main parts:

\begin{itemize}
	\item The PC controller (code written in C++)
	\item The Xilinx FPGA (code written in VHDL)
	\item The AD9910 DDS
\end{itemize}

The PC is used to program the FPGA and relay configuration data to describe the DDS output that is required. The FPGA will then convert this data into serial instructions to be programmed into the relevant DDS registers. The connection between the PC and the FPGA is controlled using a USB device, FT2232H. {\large Datasheet}

The FT2232H consists of two seperate output buses. The first, A, is connected to eight GPIO pins on the FPGA to relay data back and forth. The second, B, is connected to the JTAG programming pins of the FPGA and so allows programming of the device. It is controlled using FTD2XX.DLL and FTD2XX.h, for more information see the D2XX Programmer's Guide.

\section{Checking the Device is Connected}

The program first checks that the correct device is connected and outputs its name. If the device is wrong the output will either be garbage or blank. The most likely cause of this is lack of power to the board or the board not being given sufficient time to initialise.

\section{Programming the FPGA}

FPGA programming is achieved mainly in the FPGA.cpp file which acts as follows:

\begin{enumerate}
	\item The Done pin on the FPGA is checked, if this is high the device has been programmed and need not be reprogrammed. 
	\item The FT2232 is placed in MPSSE mode. This automatically sets up the protocol required for JTAG programming.
	\item The .svf file containing the data to be programmed is loaded and parsed using the Scan class. This is due to the .svf containing commands that make it human readable. These commands must be converted to the corresponding places in the JTAG state machine.
	\item The parsed data is sent to the device using the MPSSE.
\end{enumerate} 

Note that the MASK command is not properly implemented as it is rarely used. This results in the appearance of at least one line being read incorrectly. And may result in loss of programming function further down the line.

\section{Communication with the FPGA}

The PC sends data to the FPGA using a number of commands that control the FPGA state machine. The commands accepted by the FPGA are as follows:

\vspace{1ex}

\begin{table}
\begin{tabular}{|l|l|p{8cm}|p{4cm}|}
	\hline
	Hex & Command & Description & Output \\ \hline
	
	0x0* & SET\_LOW\_HOLD & Sends data in * to the lower nibble of the holding register. & Echoes holding register \\ \hline

	0x1* & SET\_HIGH\_HOLD & Sends data in * to the higher nibble of the holding register. Holding register is then sent to next available address in command register. & Echoes holding register \\ \hline 
	
	0x20 & READ\_DDS & Reads the contents of the DDS output buffer (vaInBuffer). Note that some contents may be zero as the whole buffer is returned despite the possibility that only part of it is filled. & Contents of DDS output buffer \\ \hline 
	
	0x26 & RAM\_RESET & Reset command RAM count and both pointers to zero. This is the state described by A) in Fig.~\ref{fig:com_RAM}. & 0xFF \\ \hline 

  	0x27 & READ\_RAM & Sequentially reads command RAM back to PC. This starts from address zero and goes up to current pointer {\large BUG} & Contents of command RAM \\ \hline 
  	
  	0x29 & READ\_TRIG\_RAM & Sequentially reads trigger RAM back to PC. This starts from address zero and reads the first ten items. (shortened for brevity whilst debugging) & Contents of the first ten trigger RAM addresses. \\ \hline 
  	
  	0x2A & TRIG\_WAIT & Go into a state waiting for an external trigger before updating the DDS registers. Another 0x2A command will cause the FPGA to leave this state. & 0x2A when leaving state \\ \hline 
  	
  	0x2D & TOGGLE\_SWEEP & Go into a state waiting for an external trigger before updating the DDS registers and continuing on to a sweeping waveform. Another 0x2D command will cause the FPGA to leave this state. & 0x2D when leaving state \\ \hline 
  	
	0x3* & SET\_PROFILE & Set the DDS profile pins to * & New state of profile pins \\ \hline 
	
	0x4* & WRITE\_FAST\_TRIG & Writes data into the trigger RAM for an instantaneous waveform change (see WRITE\_FAST\_TRIG Protocol) & The lower byte of the last address written to \\ \hline 

	0x5* & WRITE\_SWEEP\_TRIG & Writes data into the trigger RAM for a sweeping waveform (see WRITE\_SWEEP\_TRIG Protocol) & The lower byte of the last address written to \\ \hline 
\end{tabular}
\label{tab:states}
\end{table}

\subsection{Communication with the DDS}

The FPGA holds a buffer of data to be sent to the DDS in command RAM, which is written to from the PC, via the state machine, and subsequently written into the DDS, via the transfer protocol, as described in Fig.~\ref{fig:com_RAM}. 

\begin{figure}[]
\begin{center}
\includegraphics[width = 8.0cm]{Pictures/Command_RAM.png}
\caption[]{When the FPGA is first started the command RAM is empty and both the read and write pointers are at the zeroth position, this is shown in A). A command is then written to the RAM from the PC and the write pointer incremented, (B). The data is then read from the zeroth position and processed. The speed of both reading and writing varies dynamically with the program, depending on the amount of data processing required by either the PC or the FPGA. Therefore, the RAM ends up in one of two states. In C) the reading is faster than the writing and so both pointers are in the same position. This is checked by the Transfer Protocol (see Fig.~\ref{fig:FPGA}), which counts the number of valid bytes left in the RAM and pauses when there are none. In situation D) data is being written to the RAM faster than it is being read and so the writing pointer is behind the reading. This is checked by the State Machine (see Fig.~\ref{fig:FPGA}), which ensures the read pointer is not in the next address before incrementing the write pointer.}
\label{fig:com_RAM}
\end{center}
\end{figure}

Once the RAM is full of commands the RAM command write pointer will stop and wait for commands to be read before filling behind the RAM command read pointer. 


\subsection{Fast Triggering}

\vspace{-1ex}

Once the FPGA is in the WRITE\_FAST\_TRIG protocol the following data must be written to the FPGA from the PC:

\begin{enumerate}
	\item One byte giving the number of bytes to send to the DDS when trigger occurs, \(N\). (Note \(N \le 255\)) 
	\item \(N\) bytes of data to be sent on receipt of trigger
\end{enumerate}

This data, including the number of trigger bytes, is loaded into the trigger RAM.

\vspace{1ex}

Once the FPGA is placed in the TRIG\_WAIT state the number of command RAM bytes is replaced with the number of trigger bytes and all other trigger RAM is transferred into the command RAM, starting from address zero. (This assumes the writing is much faster than sending, which may not be true at the start {\large BUG?}) This is done in the trigger protocol. This means that any data still in the command RAM will be overwritten {\large BUG?}. This data is transferred as normal to the DDS device in the transfer protocol with the exception of no pulse sent on the update I/O pin. The trigger protocol will then wait until a trigger is received to switch to profile 1, this will cause an update I/O signal in the DDS. {\large This may be too specific in the triggering BUG?}.

\subsection{WRITE\_SWEEP\_TRIG Protocol}

The sweep trigger is similar to the conventional fast trigger in operation but with a number of subtle differences. Once in the WRITE\_SWEEP\_TRIG state the following data must be written via the PC:

\begin{enumerate}
	\item Two bytes giving the number of ticks to wait between each update of the waveform, LSB first. (Note must be \(\le 65535\))
	\item Two bytes giving the total number of bytes that need to be sent in the whole of the sweep, \(N\). (Note \(N \le 1019\) due to RAM constraints. This is currently the biggest constraint on the sweep.)
	\item \(N\) bytes of data to be sent across the sweep
\end{enumerate}

The second two items are loaded into trigger RAM, the number of wait ticks is held in a separate register.

\vspace{1ex}

Again the number of command RAM bytes is replaced with the number of trigger bytes and the other data in the trigger RAM moved into command RAM, starting at zero {\large as before BUG?}. This is done in the trigger protocol. Thirteen {\large BUG?} bytes of data are then transmitted to the DDS as normal via the transfer protocol but without an I/O update. Once an external trigger is detected the FPGA sends an I/O update and loads the next set of data into the DDS. The FPGA then waits a specified number of ticks before sending the next update and data. {\large This trigger may be slower than necessary, no check that data loaded before triggering}

\section{FPGA Code}

\vspace{-1ex}

An outline of the structure of the FPGA program is shown in Fig.~\ref{fig:FPGA}, the main components of which are:

\vspace{1ex}

\textbf{State Machine} This is the component which controls the flow of information between the PC and the FPGA. By sending specific commands through the USB, the PC can put the FPGA into the states shown in Table.~\ref{tab:states}.

\vspace{1ex}

The \textbf{Transfer Protocol} controls the flow of information between the data RAM and the DDS. As the data is stored in bytes it must be converted into the correct bits to be sent through the SCLCK and SDIO digital lines, for info on the protocol for these lines check the AD9910 datasheet. Data is continually transmitted whilst there are new bytes stored in the RAM. 

\vspace{1ex}

\textbf{Trigger Protocol} This copies data from the trigger RAM to the data RAM when the device is set up to trigger. As outlined in the AD9910 datasheet, the registers of the IC are only updated when the profile ID is changed, or an update pulse sent to the device. Therefore, the trigger protocol writes the data for the next waveform prior to any triggering and waits until either an external trigger or the correct amount of time has passed for an internal trigger to send an update command to the DDS.

\vspace{1ex}

\textbf{Trigger/Data RAM} Both the triggering and data RAM are 1024 bytes long. The data RAM has two address ports, one used by the PC and one by the transfer protocol. On completion of the transmission of data by the transfer protocol, new bytes are written into the vacated RAM address by the state machine. This is further described in the Communication with FPGA subsection. The trigger RAM does not require two address ports as data will only be written in preparation for the trigger and read on its receipt.

\begin{figure}[]
\begin{center}
\includegraphics[width = 8.0cm]{Pictures/FPGAinnards.png}
\caption[]{Schematic of the flow of data within the FPGA program.}
\label{fig:FPGA}
\end{center}
\end{figure}

\section{Device Capabilities}

\vspace{-1ex}

Currently, using the console UI in the C++ program the device is able to do the following:
\begin{description}
\item[Single Frequency] A tone of any frequency (up to 500 MHz) can be created with a precision of 0.2 Hz (according to the AD9910 datasheet). The amplitude can also be selected.
\item[Triangle Modulated FM] A triangle modulated waveform is created using the continuous ramping mode of the DRG on the DDS. 
\end{description}

Using the python program to create data and loading this into RAM can achieve AM and FM waveforms. An example of this is shown in option 3 of the C++ menu, which uses a file already created by the python program. To allow this to be more flexible the python program should be incorporated into C++ so that it can be accessed via the console menu.

\section{Future Improvements} 

\vspace{-1ex}

The largest required improvement to the software will be the generation of sweeping sine modulated waveforms, which will require large changes in both the VHDL and C++. My (unimplemented) plan to do this would be:
\begin{itemize}
	\item Hard-code a quarter sine wave in FPGA ROM
	\item Multiply the sine wave to match the required deviation
	\item Shift the sine wave to match the center frequency
	\item Step through the manipulated sine wave at the correct pace so that modulation frequency is correct
	\item Successively output the frequency in terms of FTWs to the DDS
	\item Re-modify the sine wave to match the changing deviation and center frequency
	\item Periodically update the ATW of the DDS output
\end{itemize}

This cannot be computed in sufficient time using the current FPGA clock as modulation frequencies of up to 1MHz may be needed whilst data is being transmitted to the DDS at 6Mbps. To speed up the calculation on the FPGA the clock speed must be increased, this can be done using a digital clock manager (DCM), provided in software by Xilinx. Multiplication and addition of the sine wave is hard in VHDL and so a processor may be needed, a small processor implementation that is suggested by Xilinx is the picoBlaze.

To achieve maximum speed for data transfer in the sine modulation sweep it may be preferable to use the parallel port, which can be clocked much faster. Bear in mind other things which may help speed up calculations:
\begin{itemize}
	\item Only the least significant figures of the FTW need change (could help speed especially in serial transmission)
	\item The step change of deviation, center frequency and amplitude can occur at a much slower rate than the transfer of FTWs. This may mean a system with two blocks of RAM, one being written do the DDS and the other being modified.
	\item The features of the DDS can be used to make transfer easier, specifically some data can be stored in the DDS RAM
\end{itemize}

Once this sine sweep system is in place the ROM can be modified to instead contain other waveforms which may be useful in other experiments.

In the far future another master FPGA card (either another board to be created or the labview card currently used by Matt) will be needed to control many V0.2 boards. Each V0.2 board will have to be programmed via USB first before being controlled by a master board. The switch between USB and master is controlled using the CPLD on V0.2, the CS REF jumpers are provided to give each slave a separate identification for the master.

Finally, a similar device has been used by a team in New Zealand to create frequencies that ramp non-linearly and instead at some predefined function. This technique may be replicated and is outlined in a thesis by Thomas McKellar, which is included in the Addtional Documents at the bitbucket.

\section{The V0.1 Prototype}

When using the V0.1 prototype ensure that the PWR\_DWN and MSTER\_RESET pins are held low for correct operation. This can be done with jumpers.

\section{The V0.2 Prototype}

\vspace{-1ex}

The V0.2 prototype contains a number of additional features, many of which have not been tested:

\begin{enumerate}
\item{CPLD} There is a CPLD between the USB, back connector and FPGA. This is designed to switch the FPGA input between USB and external control depending on a voltage input from the USB. The device is on the JTAG chain but is yet to be programmed.
\item{FPGA EEPROM} This is used to hold the programming data for the FPGA and is not yet implemented.
\item{Additional Filter Components} The components for the LC filter have not been added but guide values have been calculated in the schematic.
\item{RF Amp} This IC is yet to be tested but should be able to amplify the output of the filter.
\end{enumerate}

The Xilinx WebPack should also be able to program the CPLD and deal with the EEPROM by adding these devices to the appropriate place in the JTAG chain in software before compiling the vhdl code. This has not yet been attempted.

Jumpers are in place to bypass many of these features. The picture below shows the jumper setting for no CPLD and no EEPROM

\begin{figure}[]
\begin{center}
\includegraphics[width = 8.0cm]{Pictures/Jumpers.jpg}
\caption[]{Jumper settings for no CPLD or EEPROM}
\label{fig:Jumpers}
\end{center}
\end{figure}

To enable the CPLD the CPLD\_TDI jumper needs to be changed. To enable the EEPROM both the F\_TDO and F\_DONE jumpers must be changed. Also bear in mind the M[2:0] pins require changing, as outlined in the faults section below.

\subsection{Faults}

As the board stands the USB driver works and the INIT\_B pin on the FPGA goes high. The JTAG program appears to complete but the DONE pin does not go high. The DONE pin input to the CPLD has been disconnected on the V0.2 prototype to better test this issue.

There are a number of known hardware faults/issues on the V0.2 prototype, some of which have already been fixed on the Easy-PC layout for the next hardware iteration.
\begin{description}
\item[Silk Overlap] The silk screen overlaps in some places. (FIXED ON EASY-PC)
\item[C50, 53 and 57] These capacitors are too close together. (FIXED ON EASY-PC)
\item[AD9910 Exposed Pad] This is missing and needs to be added
\item[FT2232 Pads] This was drawn slightly too small making it difficult to solder.
\item[Labelling on Jumpers and Headers] These are badly labelled and it is hard for the user to figure out what they do.
\item[12MHz Xtal] There is not enough room on the board for this component.
\item[10MHz Oscillator] The pads for pin 3 and 4 are the wrong way round on the board.
\item[DONE pin] There is no pullup on this pin, a 330 Ohm pullup is needed. (PLACED ON PROTOTYPE)
\item[INIT\_B pin] There is no pullup on this pin, a 4K7 Ohm pullup is needed. (PLACED ON PROTOTYPE)
\item[PUDC\_B pin] There is no pulldown on this pin, a 10K Ohm pulldown is needed. (PLACED ON PROTOTYPE) Not that this pin is also subsequently used for 
\item[1.2V Power Supply] There is no 1.2V supply for the USB, another regulator is required for this.
\item[USB Ground] The 0V supply on the USB connector and the associated 0V supply on the 3.3V regulator are not properly connected to the rest of the ground on the board.
\item[L0805] These have been replaced with BLM21BD102 ferrites.
\item[C2, 5, 34, 52 and 55] These have been replaced with larger tantalum caps.
\item[The M(2:0) Config pins] These are hard wired at all low for EEPROM use, they should be changed when using EEPROM or V0.1 style JTAG. (HARDWIRED TO JTAG ON PROTOTYPE)
\end{description}

There are a number of possible issues with the board that will need to be looked into.
\begin{description}
\item[Pullups] Not all pins have pullup/downs this be an issue on the CPLD where it will cause more power to be consumed.
\item[RF Tracks] The tracks on the board carrying RF may not be at 50 Ohm impedance. This includes any digital clock signals, the REF\_CLK input and the board output
\end{description}

\section{Procedures for Modification and Maintenance}

\vspace{-1ex}

The most simple output that can be used to test that the device is working is a single frequency output. To do this call the createCW() method from main in the C++ program, followed by goToProfile(). An example of this is seen in the console menu. 

The program has not been extensively tested for long term use and may easily get stuck in a state. Two known causes for this are modifications to the C++ programming and changes in the external trigger whilst not in a trigger waiting state. To fix this, power down the device, which resets the FPGA (and erases the programming in V0.1). If this does not work in V0.2 the FPGA may have to reprogrammed as described below.

To make any modification to the VHDL programming the FPGA must be reprogrammed, this currently wont be done unless the FPGA DONE pin is low. The DONE pin becomes low when the device is powered down in V0.1 but this may not be the case in V0.2 due to the EEPROM, in this case the C++ code will have to be modified to reprogram despite the DONE pin being low.

\section{Glossary}

\vspace{-1ex}

\begin{description}
\item[V0.1 Prototype] This is the hardware that contains the AD9910 evaluation board and the Boardlevel FPGA board. Gerbers and circuit diagrams for both boards are available.
\item[V0.2 Prototype] The integrated AD9910/FPGA board. Easy-pc files are available for this board.
\end{description} 

For additional documents please visit www.bitbucket.org/Dom\_Oram/aom/src or email d.e.oram92$@$gmail.com.

\end{document}