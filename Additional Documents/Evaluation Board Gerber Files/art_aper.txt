
INPUT-UNITS     INCHES

WHEEL     1

LINE               1            D201
LINE               4            D194
LINE               5            D185
LINE               6            D188
LINE               7            D186
LINE               8            D192
LINE               9            D199
LINE              10            D189
LINE              11            D198
LINE              12            D193
LINE           12.01            D187
LINE              13            D191
LINE              14            D205
LINE              15            D190
LINE           15.75            D118
LINE              20            D196
LINE              21            D204
LINE              25            D195
LINE           25.75            D122
LINE              30            D202
LINE              50            D197
LINE              60            D203
LINE              75            D200
CIRCLE             5            D71
CIRCLE            20            D72
CIRCLE            25            D73
CIRCLE            26            D104
CIRCLE            30            D75
CIRCLE            35            D76
CIRCLE            36            D105
CIRCLE            40            D67
CIRCLE            43            D74
CIRCLE            45            D70
CIRCLE            50            D68
CIRCLE            60            D109
CIRCLE            65            D56
CIRCLE            70            D53
CIRCLE            72            D82
CIRCLE            75            D55
CIRCLE            80            D66
CIRCLE            87            D84
CIRCLE            90            D52
CIRCLE            97            D89
CIRCLE           100            D125
CIRCLE           110            D39
CIRCLE           115            D40
CIRCLE           117            D85
CIRCLE           125            D12
CIRCLE           130            D37
CIRCLE           135            D150
CIRCLE           140            D11
CIRCLE           150            D10
SQUARE            60   0.00000            D111
SQUARE            65   0.00000            D57
SQUARE            70   0.00000            D51
SQUARE            75   0.00000            D54
SQUARE            90   0.00000            D49
RECTANGLE       8.66     55.12   0.00000  D135
RECTANGLE         10        65   0.00000  D42
RECTANGLE         10     72.01   0.00000  D59
RECTANGLE      11.81     59.06   0.00000  D127
RECTANGLE      11.81      68.9   0.00000  D96
RECTANGLE         13        65   0.00000  D46
RECTANGLE         15        70   0.00000  D44
RECTANGLE         15     77.01   0.00000  D61
RECTANGLE         18        70   0.00000  D48
RECTANGLE      18.66     65.12   0.00000  D138
RECTANGLE         20        25   0.00000  D171
RECTANGLE      21.81     69.06   0.00000  D130
RECTANGLE      21.81      78.9   0.00000  D99
RECTANGLE         25        20   0.00000  D170
RECTANGLE         30        35   0.00000  D174
RECTANGLE         30        38   0.00000  D166
RECTANGLE         30        55   0.00000  D142
RECTANGLE         30        90   0.00000  D175
RECTANGLE         32        35   0.00000  D22
RECTANGLE         35        30   0.00000  D173
RECTANGLE         35        32   0.00000  D21
RECTANGLE         37        40   0.00000  D24
RECTANGLE         38        30   0.00000  D165
RECTANGLE         40        37   0.00000  D23
RECTANGLE         40        48   0.00000  D169
RECTANGLE         40        65   0.00000  D145
RECTANGLE         40       100   0.00000  D178
RECTANGLE         43        47   0.00000  D27
RECTANGLE         47        43   0.00000  D28
RECTANGLE         48        40   0.00000  D168
RECTANGLE         48        52   0.00000  D29
RECTANGLE         50        60   0.00000  D181
RECTANGLE         51        63   0.00000  D33
RECTANGLE         52        48   0.00000  D30
RECTANGLE         52        55   0.00000  D20
RECTANGLE         55        30   0.00000  D143
RECTANGLE         55        52   0.00000  D19
RECTANGLE      55.12      8.66   0.00000  D136
RECTANGLE         56        68   0.00000  D35
RECTANGLE      59.06     11.81   0.00000  D128
RECTANGLE         60        50   0.00000  D180
RECTANGLE         60        70   0.00000  D184
RECTANGLE         63        51   0.00000  D34
RECTANGLE         63        79   0.00000  D15
RECTANGLE      64.96       100   0.00000  D113
RECTANGLE         65        10   0.00000  D41
RECTANGLE         65        13   0.00000  D45
RECTANGLE         65        40   0.00000  D146
RECTANGLE      65.12     18.66   0.00000  D139
RECTANGLE         68        56   0.00000  D36
RECTANGLE         68        84   0.00000  D17
RECTANGLE       68.9     11.81   0.00000  D97
RECTANGLE      69.06     21.81   0.00000  D131
RECTANGLE         70        15   0.00000  D43
RECTANGLE         70        18   0.00000  D47
RECTANGLE         70        60   0.00000  D183
RECTANGLE         71        83   0.00000  D31
RECTANGLE      72.01        10   0.00000  D58
RECTANGLE      74.96       110   0.00000  D116
RECTANGLE      77.01        15   0.00000  D60
RECTANGLE         78     216.5   0.00000  D62
RECTANGLE      78.74    216.54   0.00000  D153
RECTANGLE       78.9     21.81   0.00000  D100
RECTANGLE         79        63   0.00000  D16
RECTANGLE         80       120   0.00000  D159
RECTANGLE         83        71   0.00000  D32
RECTANGLE         83        87   0.00000  D25
RECTANGLE         83        99   0.00000  D13
RECTANGLE         83     221.5   0.00000  D64
RECTANGLE         84        68   0.00000  D18
RECTANGLE         87        83   0.00000  D26
RECTANGLE         87       102   0.00000  D81
RECTANGLE      88.74    226.54   0.00000  D156
RECTANGLE         90        30   0.00000  D176
RECTANGLE         90       130   0.00000  D162
RECTANGLE         95       102   0.00000  D93
RECTANGLE         97       112   0.00000  D88
RECTANGLE         99        83   0.00000  D14
RECTANGLE        100        40   0.00000  D179
RECTANGLE        100     64.96   0.00000  D114
RECTANGLE        100       107   0.00000  D95
RECTANGLE        102        87   0.00000  D80
RECTANGLE        102        95   0.00000  D92
RECTANGLE        107       100   0.00000  D94
RECTANGLE        110     74.96   0.00000  D117
RECTANGLE        112        97   0.00000  D87
RECTANGLE        120        80   0.00000  D160
RECTANGLE        127       132   0.00000  D78
RECTANGLE        130        90   0.00000  D163
RECTANGLE        132       127   0.00000  D77
RECTANGLE        135       142   0.00000  D91
RECTANGLE        142       135   0.00000  D90
RECTANGLE      216.5        78   0.00000  D63
RECTANGLE     216.54     78.74   0.00000  D152
RECTANGLE      221.5        83   0.00000  D65
RECTANGLE     226.54     88.74   0.00000  D155
OBLONG         15.75     43.31   0.00000  D119
OBLONG         25.75     53.31   0.00000  D123
OBLONG         43.31     15.75   0.00000  D120
OBLONG         53.31     25.75   0.00000  D124
FLASH     -1                   0.00000  D107
FLASH     -2                   0.00000  D106
FLASH     0                    0.00000  D108
FLASH     CIRCLE5              0.00000  D69
FLASH     PAD1                 0.00000  D182
FLASH     PAD10                0.00000  D164
FLASH     PAD11                0.00000  D161
FLASH     PAD12                0.00000  D157
FLASH     PAD13                0.00000  D158
FLASH     PAD14                0.00000  D154
FLASH     PAD18                0.00000  D151
FLASH     PAD19                0.00000  D149
FLASH     PAD2                 0.00000  D177
FLASH     PAD23                0.00000  D147
FLASH     PAD24                0.00000  D148
FLASH     PAD25                0.00000  D144
FLASH     PAD26                0.00000  D141
FLASH     PAD27                0.00000  D102
FLASH     PAD28                0.00000  D103
FLASH     PAD29                0.00000  D98
FLASH     PAD3                 0.00000  D172
FLASH     PAD30                0.00000  D101
FLASH     PAD33                0.00000  D140
FLASH     PAD34                0.00000  D137
FLASH     PAD35                0.00000  D134
FLASH     PAD36                0.00000  D133
FLASH     PAD37                0.00000  D132
FLASH     PAD38                0.00000  D129
FLASH     PAD4                 0.00000  D167
FLASH     PAD40                0.00000  D126
FLASH     PAD44                0.00000  D121
FLASH     PAD46                0.00000  D115
FLASH     PAD47                0.00000  D110
FLASH     PAD48                0.00000  D112
FLASH     T-REL-100            0.00000  D83
FLASH     T13011025X45SP4      0.00000  D38
FLASH     T907015X45SP4        0.00000  D50
FLASH     TC87S15              0.00000  D86
FLASH     TR102X87S15          0.00000  D79

