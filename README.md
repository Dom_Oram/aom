# README #

This project aims to build a flexible RF generator using the AD9910 DDS device and a Xilinx FPGA.

For a detailed description of the project please see the technical document in Additional Files.

For any questions please contact Dominic Oram at d.e.oram92@gmail.com