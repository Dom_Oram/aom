import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
    
profiles = []

#Needs to be more accurate
CLOCK = 40*24.998364e6
TIME_RES = 4/CLOCK

contains_AM     = False

class Profile:

    def __init__(self, profile_type, start_address, end_address, data):#, predicted_output, predicted_timestep):
        self.prof_type = profile_type
        self.start_add = start_address
        self.end_add = end_address
        self.RAM_data = data
        #self.predict = predicted_output
        #self.predict_time = float(predicted_timestep)

    def __str__(self):
        return "   Type : " + self.prof_type + "\n   Start Address: " + str(self.start_add) + "\n   End Address: " + str(self.end_add)

    def getEnd(self):
        return self.end_add

    def getPredict(self):
        time = np.arange(0, len(self.predict)*float(self.predict_time), float(self.predict_time))
        return [time, self.predict]

    def toFile(self):
        return self.RAM_data
    
def reFormat(out):
    #put them in format of Word[31:18]
    #(FFFC0000)    
    #convert to binary
    string_out = np.empty((1023,1), dtype=(np.str,32))

    for i in range(0,1023):
        string_out[i] = '00000000000000000000000000000000'

    it = np.nditer(out, flags=['f_index'])
    while not it.finished:
        string_out[it.index] = np.binary_repr(int(it[0]),32)   
        it.iternext()
        
    return string_out

def saveAsHex(out):
    string_out = bytearray(1023*4)
    
    it = np.nditer(out, flags=['f_index'])
    while not it.finished:
        tempInt     =   int(it[0])

        string_out[it.index*4+3]    = int((tempInt & 0xFF000000) >> 24)
        string_out[it.index*4+2]    = int((tempInt & 0x00FF0000) >> 16)
        string_out[it.index*4+1]    = int((tempInt & 0x0000FF00) >> 8)
        string_out[it.index*4]    = int((tempInt & 0x000000FF))       

        it.iternext()

    print string_out

    with open('RAM_hex.txt', 'wb') as output:
        output.write(string_out)
        
def createSingleTone(start):
    freq = float(raw_input("Please enter the wave frequency: "))

    out = np.ones((1))
    out[0] = calcFTW(freq)

    profile = Profile("c.w.", start, start + 1, out)
    
    return profile

def createRamp(start):
    start_freq = float(raw_input("Please enter the starting frequency: "))
    end_freq = float(raw_input("Please enter the ending frequency: "))
    ramp_time = float(raw_input("Please enter the time the ramp should take: "))

    start_freq_FTW  = calcFTW(start_freq)
    end_freq_FTW    = calcFTW(end_freq) 

    num = ramp_time/TIME_RES

    num = checkMemOverflow(start, num)

    out = np.linspace(start_freq_FTW, end_freq_FTW, num)
    
    #expected = 
    
    return Profile("Ramp", start, start + num - 1, out)
    
def checkMemOverflow(start, num):
    multi    = 1
    original_num = num
    while (num > 1000): #WARNING: this needs changing
        multi += 1
        num = int(original_num/multi)
        print multi, num
        
    if multi > 1:
        print "Set RAM timestep to: " + str(multi) + " steps"

    return num

def createAM(start_address):
    if (contains_AM):
        print "Note: AM wave must have same carrier"

    freq_carrier = float(raw_input("Please enter the carrier frequency: "))
    freq_modulation = float(raw_input("Please enter the modulation frequency: "))
    #timestep = float(raw_input("Please enter RAM timestep: "))
    
    #if (freq_carrier < freq_modulation):
        #print "Modulation frequency cannot be higher than carrier"
        #return 0
        
    #num_of_values = int(freq_carrier/(2*freq_modulation))

    num_of_values = int(1/(2*freq_modulation*TIME_RES))

    num_of_values = checkMemOverflow(start_address, num_of_values)

    print "Remember to set FTW register to: " + str(calcFTW(freq_carrier))

    #create the amplitude values
    out = np.linspace(-num_of_values/2, num_of_values/2, num_of_values+1)
    out *= np.pi/num_of_values
    out = 0.5 + np.sin(out)/2.0

    plt.plot(out)
    plt.show()

    #np.save("Expected AM", [expected_time, expected_out])

    #plt.plot(expected_time, expected_out)
    #plt.show()

    #first value needs to be 2^18 then up to 2^31
    #output nonlinear at > 13500
    out *= 14500
    out *= pow(2,18)

    #plt.plot(out)
    #plt.show()

    profile = Profile("AM", start_address, start_address + num_of_values, out)#, expected_out, expected_time)

    return profile

def calcFTW(frequency):
    
    return int(round(pow(2,32)*frequency/CLOCK))

def createFM(car, mod_wf, dev, timestep, mod):
    out = np.zeros_like(mod_wf)

    integ = np.zeros_like(mod_wf)

    integ[-1] = -1.0

    samples = len(mod_wf)

    expected_time   = np.linspace(0, samples-1, samples)*float(timestep)
    
    #integ = 0.0

    for i in range (0, samples):
        integ[i] = integ[i-1]+ mod_wf[i]*mod
        #print integ[i], mod_wf[i]
        #integ = mod_wf[i]
        
        analog_out = np.pi*2.0*timestep*i*(car + dev*mod_wf[i])
    
        out[i] = np.cos(analog_out)

    repeats = 10
    samples *= repeats

    out = np.tile(out, repeats)
    expected_time   = np.linspace(0, samples-1, samples)*float(timestep)

    #plt.plot(expected_time[:samples/repeats], mod_wf)    
    plt.plot(expected_time[:samples/repeats], out[:samples/repeats])
    plt.show()

   
             
def createNoiseFM(car, dev):
    samples = 500000

    #mod = 3.0/50*dev

    mod = 1e6

    time_period = 5/mod

    timestep = float(time_period/samples)
    
    #rand = np.random.uniform(0.0,1.0,samples)*mod

    #rand = np.real(np.fft.ifft(rand))

    expected_time   = np.linspace(0, samples-1, samples)*float(timestep)

    rand = np.sin(2.0*np.pi*expected_time*mod)

    #redifne mod to contain all prefactors
    mod *= 2.0*np.pi*timestep

    createFM(car, rand, dev, timestep, mod)


def createTriangleFM(start_address):

    freq_carrier = float(raw_input("Please enter the carrier frequency: "))
    freq_modulation = float(raw_input("Please enter the modulation frequency: "))
    freq_dev = float(raw_input("Please enter the frequency deviation: "))

    num_of_values = int(1/(2*freq_modulation*TIME_RES))

    num_of_values = checkMemOverflow(start_address, num_of_values)

    times = np.linspace(0, 1, num_of_values+1) 

    out = signal.sawtooth(2 * np.pi * times) #ramp up from -dev to dev

    plt.plot(times, out)
    plt.show()

    FTW_max = calcFTW(freq_dev)
    out *= FTW_max

    #apply to carrier
    FTW_carrier = calcFTW(freq_carrier)
    out += FTW_carrier

    profile = Profile("FM", start_address, start_address + num_of_values, out)#, expected_out, expected_time)

    return profile

def createSawtoothFM(car, dev):
    samples = 100000

    car -= dev

    mod = 3.0/50*dev

    time_period = 1/mod

    timestep = float(time_period/samples)

    expected_time   = np.linspace(0, samples-1, samples)*float(timestep)

    rand = signal.sawtooth(2 * np.pi * mod * expected_time)

    #redifne mod to contain all prefactors
    mod *= 2.0*np.pi*timestep

    createFM(car, rand, dev, timestep, mod)

def createFMfromInput(start_address):

    freq_carrier = float(raw_input("Please enter the carrier frequency: "))
    freq_modulation = float(raw_input("Please enter the modulation frequency: "))
    freq_dev = float(raw_input("Please enter the frequency deviation: "))
    #timestep = float(raw_input("Please enter RAM timestep: "))
    
    num_of_values = int(1/(freq_modulation*TIME_RES))

    num_of_values = checkMemOverflow(start_address, num_of_values)
    print num_of_values
    
    #create the frequency deviation values
    out = np.linspace(-num_of_values/2, num_of_values/2, num_of_values+1)
    out *= np.pi/(num_of_values)
    out = np.sin(out)
  
    FTW_max = calcFTW(freq_dev)
    out *= FTW_max

    #apply to carrier
    FTW_carrier = calcFTW(freq_carrier)
    out += FTW_carrier

    #plt.savefig('FM Theory/(' + str(freq_carrier*1e-6) + 'MHz, ' + str(freq_modulation*1e-6) +'MHz, ' + str(freq_dev*1e-6) + 'MHz) Fourier Difference.png')

    #plt.show()

    profile = Profile("FM", start_address, start_address + num_of_values, out)#, expected_out, expected_time)

    return profile

option = 0
start = 0

while(option != 3):
    print "Please select output mode: "
    print "1. Single Tone"
    print "2. Ramp"
    print "3. FM-Sin"
    print "4. FM-Tri"
    print "5. AM-Sin"
    print "6. Print FTW"

    mode = int(raw_input())
    temp_profile = 0
    #if (mode < 4 and contains_AM):
        #print "RAM cannot contain AM and FM waves, please select different output"
    if mode == 1:
        temp_profile = createSingleTone(start)
    elif mode == 2:
        temp_profile = createRamp(start)
    elif mode == 3:
        temp_profile = createFMfromInput(start)
    elif mode == 4:
        temp_profile = createTriangleFM(start)
    elif mode == 5:
        temp_profile = createAM(start)
    elif mode == 6:
        freq = float (raw_input("Freq: "))
        print int (calcFTW(freq))
    else:
        print "Invalid option"

    if temp_profile != 0:
        start = temp_profile.getEnd()
        profiles.append(temp_profile)

        for i in range(len(profiles)):
            print "Profile " + str(i) + ":"
            print str(profiles[i])

        print "1. Add New Profile"
        print "2. Plot Predicted Output"
        print "3. Save and Exit"

        option = int(raw_input())

        while (option == 2):
            for item in profiles:
                prediction = item.getPredict()
                plt.plot(prediction[0], prediction[1])
                plt.show()

            option = int(raw_input())

out = profiles[0].toFile()

for i in range(1, len(profiles)):
    out = np.concatenate([out, profiles[i].toFile()])

saveAsHex(out)

string_out = reFormat(out)

np.savetxt("RAM.txt", string_out, fmt = '%s')
        
