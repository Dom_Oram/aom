----------------------------------------------------------------------------------
--  
-- USBProg - USBProg module
--
-- Target architecture : XC3S200A-4VQ100
--
-- Copyright (c) 2012 Board Level Limited
-- All rights reserved
--
-- www.boardlevel.co.uk
--
-- Revision history:
--
-- 10/04/12 NR : Initial release
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.vcomponents.all;

entity USBProg is
  port (
    -- Host port
    pCLK            : in    STD_LOGIC;                      -- 12.0 MHz system clock
    pnRXRDY         : in    STD_LOGIC;                      -- LOW when Rx data available
    pnTXRDY         : in    STD_LOGIC;                      -- LOW when Tx space available
    pX              : in    STD_LOGIC;
    pnINIT          : out   STD_LOGIC;
    pnRD            : out   STD_LOGIC;                      -- LOW to enable Rx data onto D[0..7] and pop the Rx FIFO
    pWR             : out   STD_LOGIC;
    pSI_WU          : out   STD_LOGIC;
    paD             : inout STD_LOGIC_VECTOR (7 downto 0);
    
    -- Voltage sources
    pSDI            : in    STD_LOGIC;
    pSDO            : out   STD_LOGIC;
    pSCK            : out   STD_LOGIC;
    pnCSADC         : out   STD_LOGIC;
    pnCSV1          : out   STD_LOGIC;
    pnCSV2          : out   STD_LOGIC;
    pMCLR           : out   STD_LOGIC;
    p15V_EN         : out   STD_LOGIC;
    

    paTI            : in    STD_LOGIC_VECTOR (3 downto 0);
    pnCSWREG        : out   STD_LOGIC;    
    paTOE           : out   STD_LOGIC_VECTOR (11 downto 0);
		paTOE12					: out		STD_LOGIC;
		paTOE13					: out		STD_LOGIC;
		paTOE14					: out		STD_LOGIC;
		paTOE16					: out		STD_LOGIC;
    paTOE18         : out   STD_LOGIC;
    paTOE19         : out   STD_LOGIC;
    paTOE20         : out   STD_LOGIC;
    paTOE22         : out   STD_LOGIC;
		
		paTIO9				: in		STD_LOGIC; --TESTING
    paTIO         : inout STD_LOGIC_VECTOR (21 downto 0)
		
  );
end USBProg;
 
architecture Behavioral of USBProg is

  -----------------------------------------------------------------------------
  -- Constant declarations
  -----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------
  -- Function declarations
  -----------------------------------------------------------------------------
  
  -----------------------------------------------------------------------------
  -- Component declarations
  -----------------------------------------------------------------------------
			
  -----------------------------------------------------------------------------
  -- Signal declarations
  -----------------------------------------------------------------------------

    -- Port signals
    signal sINIT       : STD_LOGIC;
    signal sRD         : STD_LOGIC;
    signal sWR         : STD_LOGIC;
    signal sSI_WU      : STD_LOGIC;
    signal saD         : STD_LOGIC_VECTOR (7 downto 0);
    signal sSDO        : STD_LOGIC;
    signal sSCK        : STD_LOGIC;
    signal sCSADC      : STD_LOGIC;
    signal sCSV1       : STD_LOGIC;
    signal sCSV2       : STD_LOGIC;
    signal sMCLR       : STD_LOGIC;
    signal s15V_EN     : STD_LOGIC;
    signal sCSWREG     : STD_LOGIC;
    signal saTOE       : STD_LOGIC_VECTOR (23 downto 0);
    signal saTIO       : STD_LOGIC_VECTOR (21 downto 0);
		signal saTIO9			 : STD_LOGIC;
    signal saCount     : STD_LOGIC_VECTOR (7 downto 0);

    -- Local signals
    signal saDebug     : STD_LOGIC_VECTOR (15 downto 0);
    signal saHoldData  : STD_LOGIC_VECTOR (7 downto 0);
    signal saHostTxD   : STD_LOGIC_VECTOR (7 downto 0);
    signal sBusy       : STD_LOGIC;
    signal sWDReady    : STD_LOGIC;
    signal sACK        : STD_LOGIC;
    signal sTxEnable   : STD_LOGIC;
    signal sSDAOut     : STD_LOGIC;
    signal sSCLOut     : STD_LOGIC;
    signal sSDAIn      : STD_LOGIC;
    signal sWRegReady  : STD_LOGIC;
		
		-- RAM signals
		signal rAddA					:	STD_LOGIC_VECTOR (10 downto 0); 
		signal rDataOutA			: STD_LOGIC_VECTOR (7 downto 0);
		signal rDataInA				: STD_LOGIC_VECTOR (7 downto 0);
		signal rClkA					: STD_LOGIC;
		signal rEnA						:	STD_LOGIC;
		signal rWrA						: STD_LOGIC;
		
		signal rAddB					:	STD_LOGIC_VECTOR (10 downto 0);			
		signal rDataOutB			: STD_LOGIC_VECTOR (7 downto 0);
		signal rClkB					: STD_LOGIC;
		signal rEnB						:	STD_LOGIC;
		signal rWrB						: STD_LOGIC;
		
		-- Trig RAM signals
		signal tOut						: STD_LOGIC_VECTOR (7 downto 0); 	-- 8-bit RAM data output
		--signal tProf 					: STD_LOGIC_VECTOR (3 downto 0);  -- Profile that the RAM is triggering to
		signal tAdd						: STD_LOGIC_VECTOR (10 downto 0); 	-- Address of data in tProf
		signal tIn						: STD_LOGIC_VECTOR (7 downto 0); 	-- 8-bit RAM data input
		signal tClk 	 				: STD_LOGIC;											-- Write clock input
		signal tWE 						: STD_LOGIC;											-- Write enable input
		
		-- DDS I/O
		signal sSDC						:	STD_LOGIC;
		signal sSDOut					:	STD_LOGIC;
		signal sSDIn					:	STD_LOGIC;
		signal sIOup					:	STD_LOGIC;
		signal sIOres					:	STD_LOGIC;
		signal sReadSDIO			: STD_LOGIC;
		signal sProfile				:	STD_LOGIC_VECTOR (2 downto 0);
		signal sTrigger 			: STD_LOGIC;
		signal sDRctrl				: STD_LOGIC;
		
  -----------------------------------------------------------------------------
  -- Implementation
  -----------------------------------------------------------------------------
  begin

    -----------------------------------------------------------------------------
    -- Port signal mapping
    -----------------------------------------------------------------------------
			
			--RAM for general data
			
			RAMB16_S9_S9_inst : RAMB16_S9_S9
			generic map (
				INIT_A => X"000", -- Value of output RAM registers on Port A at startup
				INIT_B => X"000", -- Value of output RAM registers on Port B at startup
				SRVAL_A => X"000", -- Port A ouput value upon SSR assertion
				SRVAL_B => X"000", -- Port B ouput value upon SSR assertion
				WRITE_MODE_A => "READ_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
				WRITE_MODE_B => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
				SIM_COLLISION_CHECK => "ALL", -- "NONE", "WARNING", "GENERATE_X_ONLY", "ALL"
				-- The following INIT_xx declarations specify the initial contents of the RAM
				-- Address 0 to 511
				INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
				-- Address 512 to 1023
				INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
				-- Address 1024 to 1535
				INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
				INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
				-- The next set of INITP_xx are for the parity bits
				-- Address 0 to 511
				INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
				-- Address 512 to 1023
				INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
				-- Address 1024 to 1535
				INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
				-- Address 1536 to 2047
				INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
				INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
			port map (
				DOA => rDataOutA, -- Port A 8-bit Data Output
				DOB => rDataOutB, -- Port B 8-bit Data Output
				DIPA => "0", -- Port A 1-bit Parity In
				DIPB => "0", -- Port B 1-bit Parity OIn
				ADDRA => rAddA, -- Port A 11-bit Address Input
				ADDRB => rAddB, -- Port B 11-bit Address Input
				CLKA => rClkA, -- Port A Clock
				CLKB => rClkB, -- Port B Clock
				DIA => rDataInA, -- Port A 8-bit Data Input
				DIB => "00000000", -- Port B 8-bit Data Input
				ENA => rEnA, -- Port A RAM Enable Input
				ENB => rEnB, -- PortB RAM Enable Input
				SSRA => '0', -- Port A Synchronous Set/Reset Input
				SSRB => '0', -- Port B Synchronous Set/Reset Input
				WEA => rWrA, -- Port A Write Enable Input
				WEB => rWrB -- Port B Write Enable Input
			);
			-- End of RAMB16_S9_S9_inst instantiation
	
      -- N.B. signals  saTOE(23), saTOE(21) and saTOE(17 downto 12) are not
      -- brought out to external pins
      -- They must be exported indirectly using the external register
      --
      
			--RAM to hold triggering data
			RAMB16_S9_inst : RAMB16_S9
			generic map (
			INIT => X"000", -- Value of output RAM registers at startup
			SRVAL => X"000", -- Ouput value upon SSR assertion
			WRITE_MODE => "WRITE_FIRST", -- WRITE_FIRST, READ_FIRST or NO_CHANGE
			-- The following INIT_xx declarations specify the initial contents of the RAM
			-- Address 0 to 511
			INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
			-- Address 512 to 1023
			INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
			-- Address 1024 to 1535
			INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
			-- Address 1536 to 2047
			INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
			INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
			-- The next set of INITP_xx are for the parity bits
			-- Address 0 to 511
			INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
			-- Address 512 to 1023
			INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
			-- Address 1024 to 1535
			INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
			-- Address 1536 to 2047
			INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
			INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000")
			port map (
				DO 		=> tOut, 							-- 8-bit data output
				ADDR  => tAdd, 							-- 11-bit address input
				CLK 	=> tClk, 							-- Clock input
				DI 		=> tIn, 							-- 8-bit data input
				DIP		=> "0",
				EN 		=> '1', 							-- RAM enable input
				SSR 	=> '0', 							-- Synchronous reset input
				WE 		=> tWE 								-- RAM write enable input
			);
			-- End of RAMB4_S8_inst instantiation
			
      pnINIT             <= not sINIT;
      pnRD               <= not sRD;
      pWR                <= sWR;
      pSI_WU             <= sSI_WU;
      paD                <= saD;
      pSDO               <= sSDO;
      pSCK               <= sSCK;
      pnCSADC            <= not sCSADC;
      pnCSV1             <= not sCSV1;
      pnCSV2             <= not sCSV2;
      pMCLR              <= sMCLR;
      p15V_EN            <= s15V_EN;
      pnCSWREG           <= not sCSWREG;		
      paTOE(11 downto 0) <= saTOE(11 downto 0);
			paTOE12							<= saTOE(12);
			paTOE13							<= saTOE(13);
			paTOE14							<= saTOE(14);
			paTOE16							<= saTOE(16);
      paTOE18            <= saTOE(18);
      paTOE19            <= saTOE(19);
      paTOE20            <= saTOE(20);
      paTOE22            <= saTOE(22);
      paTIO              <= saTIO;
			
						
      -- Pullups
      cTIO0Pullup      : PULLUP port map (O => saTIO(0));
      cTIO1Pullup      : PULLUP port map (O => saTIO(1));
      cTIO2Pullup      : PULLUP port map (O => saTIO(2));
      cTIO3Pullup      : PULLUP port map (O => saTIO(3));
      cTIO4Pullup      : PULLUP port map (O => saTIO(4));
      cTIO5Pullup      : PULLUP port map (O => saTIO(5));
      cTIO6Pullup      : PULLUP port map (O => saTIO(6));
      cTIO7Pullup      : PULLUP port map (O => saTIO(7));
      cTIO8Pullup      : PULLUP port map (O => saTIO(8));
      --cTIO9Pullup      : PULLUP port map (O => saTIO9);
      cTIO10Pullup     : PULLUP port map (O => saTIO(10));
      cTIO11Pullup     : PULLUP port map (O => saTIO(11));
      cTIO12Pullup     : PULLUP port map (O => saTIO(12));
      cTIO13Pullup     : PULLUP port map (O => saTIO(13));
      cTIO14Pullup     : PULLUP port map (O => saTIO(14));
      cTIO15Pullup     : PULLUP port map (O => saTIO(15));
      cTIO16Pullup     : PULLUP port map (O => saTIO(16));
      cTIO17Pullup     : PULLUP port map (O => saTIO(17));
      cTIO18Pullup     : PULLUP port map (O => saTIO(18));
      cTIO19Pullup     : PULLUP port map (O => saTIO(19));
      cTIO20Pullup     : PULLUP port map (O => saTIO(20));
      cTIO21Pullup     : PULLUP port map (O => saTIO(21));

    -----------------------------------------------------------------------------
    -- Host data transfer
    -----------------------------------------------------------------------------

      process
        begin
          wait until pCLK'event and pCLK = '1';						
          if (sRD = '1') or (sWR = '1') or (sCSWREG = '1') then
            -- Terminate the data transfer
            sRD     <= '0';
            sWR     <= '0';
            sCSWREG <= '0';
            saD     <= (others => 'Z');
          elsif sWRegReady = '1' then
            -- Write request for the external register
            sCSWREG <= '1';
            saD(0)  <= saTOE(23);
            saD(1)  <= saTOE(16);
            saD(2)  <= saTOE(15);
            saD(3)  <= saTOE(12);
            saD(4)  <= saTOE(13);
            saD(5)  <= saTOE(14);
            saD(6)  <= saTOE(21);
            saD(7)  <= saTOE(17);
          elsif (pnTXRDY = '0') and (sWDReady = '1') then
            -- Write request for the USB interface
            sWR     <= '1';
            saD     <= saHostTxD;
          elsif (pnRXRDY = '0') and (sBusy = '0') then
            -- Fetch a command byte from the USB interface
            sRD     <= '1';
            saD     <= (others => 'Z');
          end if;
        end process;
        
    -----------------------------------------------------------------------------
    -- Command processor
    -----------------------------------------------------------------------------
        
      process
				
        type tState is (
          STATE_IDLE, 
          STATE_ECHO_HOLD,		
					STATE_READ_DDS,			--0x20
					STATE_RAM_RESET,		--0x26
					STATE_HOLD_TO_RAM,	
					STATE_READ_RAM,			--0x27
					STATE_READ_TRIG,	 	--0x29
					STATE_TRIG_WAIT, 		--0x2A
					STATE_SET_PROFILE,	--0x3X
					STATE_WRITE_TRIG,   --0x4X or 0x5X for sweeping change
					STATE_TOGGLE_SWEEP,	--0x2D
					STATE_REPLY_WAIT
        );
      
        variable vaState 			: tState                        := STATE_IDLE;
        variable vaCount 			: STD_LOGIC_VECTOR (12 downto 0) := (others => '0'); --used in many places for generic counting
				variable vaRamCount		:	STD_LOGIC_VECTOR (10 downto 0) := (others => '0'); --how many bytes are in command RAM
				variable vaHold  			: STD_LOGIC_VECTOR (7 downto 0) := (others => '0');	--holds pc commands before they are placed in command RAM 		
				
				variable vaNumBytesOut		: STD_LOGIC_VECTOR (12 downto 0) := (others => '0'); --how many bytes each DDS instruction requires
				variable vaReadInstr			:	STD_LOGIC := '0';																	--whether the instruction to the DDS is read or write
 				variable vaSend						:	STD_LOGIC_VECTOR (6 downto 0) := (others => '0'); --which bit of RAM sent
				variable vaOutByte				: UNSIGNED (7 downto 0) := (others => '0');					--storage of byte to send to DDS
				variable vaInBuffer				: UNSIGNED (63 downto 0) := (others => '0'); 				--storage of bytes returned from DDS
				variable vaInBufferFull		:	STD_LOGIC := '0';	
				variable vaUpdateNeeded		:	STD_LOGIC := '0';
				variable vaUpCount				:	STD_LOGIC_VECTOR (7 downto 0) := (others => '0'); --used to hold I/O Update
				
				variable vaTriggerLive		: STD_LOGIC := '0'; --high when waiting for trigger
				variable vaCopyTrigger		: STD_LOGIC := '0'; --high when there is data to copy from trigger RAM into transmission RAM
				variable vaCopyCount 			: STD_LOGIC_VECTOR (7 downto 0) := (others => '0'); --used to count when copying RAM
				variable vaTrigBytes			: STD_LOGIC_VECTOR (10 downto 0) := (others => '0'); --used to hold the number of bytes in trig
				
				variable vaSweepOn					:	STD_LOGIC := '0'; --high when sending a sweeping w.f.
				variable vaRampBytesSent	:	STD_LOGIC_VECTOR (4 downto 0) := (others => '0'); --counts the number of bytes sent to DDS when sweeping
				variable vaWaitNeeded			: STD_LOGIC_VECTOR (15 downto 0) := (others => '0'); --clocks between sweep changes
				variable vaWaitTemp				: STD_LOGIC_VECTOR (15 downto 0) := (others => '0'); --used to count clocks
				variable vaIsSweep					: STD_LOGIC := '0'; --high when writing data for a sweeping w.f
								
				begin
          wait until pClk'event and pClk = '1';

					-----------------------------------------------------------------------------
					-- State machine
					-----------------------------------------------------------------------------						
					case vaState is
            when STATE_IDLE =>
              --saDebug(3 downto 0) <= "0000";
						
              sBusy      	<= sRD;
              sWDReady   	<= '0';
              sWRegReady 	<= '0';
							
							rEnA				<= '0';
							rWrA				<= '0';
							rClkA				<= '0';
							
							tWE							<= '0';
							tClk						<= '0';
							tAdd						<= (others => '0');
							vaTriggerLive		:= '0';
							sDRctrl					<= '1';
							
              vaCount    	:= (others => '0');
							vaHold		 	:= (others => '0');
							
              if sRD = '1' then
                if saD(7 downto 5) = 0 then
                  if saD(4) = '1' then
                    --set hold upper nibble
										saHoldData(7 downto 4) <= saD(3 downto 0);
										--send hold to RAM								
                    vaState := STATE_HOLD_TO_RAM;
                  else
										--set hold lower nibble
                    saHoldData(3 downto 0) <= saD(3 downto 0);
										--echo hold
                    vaState := STATE_ECHO_HOLD;
                  end if;
                else
									if saD(7 downto 4) = "0011" then
										vaState := STATE_SET_PROFILE;
									
									elsif saD(7 downto 5) = "010" then
										vaState := STATE_WRITE_TRIG;
										
									else
										case saD is
											when "00100000" =>
												vaState := STATE_READ_DDS;
											when "00100110" =>
												vaState := STATE_RAM_RESET;
											when "00100111" =>
												rAddA <= (others => '0');
												vaState := STATE_READ_RAM;
											when "00101001" =>
												vaState := STATE_READ_TRIG;	
											
											when "00101010" =>
												vaState := STATE_TRIG_WAIT;
											
											when "00101101" =>
												vaState := STATE_TOGGLE_SWEEP;
												
											when others =>
												null;
										end case;
									end if;
								end if;
              end if;
              
            when STATE_ECHO_HOLD =>
              --saDebug(3 downto 0) <= "0001";
              saHostTxD <= saHoldData;
              sWDReady  <= '1';
              vaState   := STATE_REPLY_WAIT;
						
						when STATE_RAM_RESET =>
							--saDebug(3 downto 0) <= "1001";
							--reset both pointers and count to zero
							rAddA 			<= (others => '0');
							rAddB 			<= (others => '0');
							vaRamCount 	:= (others => '0');
							saHostTxD 	<= (others => '1');
							sWDReady  	<= '1';
							vaState   	:= STATE_REPLY_WAIT;

						when STATE_HOLD_TO_RAM =>
							--saDebug(3 downto 0) <= "1010";
							rEnA 			<= '1';
							rWrA			<= '1';
							rDataInA 	<= saHoldData;
							--fill rAddA (write pointer) behind rAddB (read pointer)
							if not (STD_LOGIC_VECTOR(unsigned(rAddB)) - STD_LOGIC_VECTOR(unsigned(rAddA)) = 1) then 
								if (vaCount = 1) then
									rClkA 			<= '1';								
								end if;
								if (vaCount = 2) then
									rClkA				<= '0';
									rAddA				<= STD_LOGIC_VECTOR(unsigned(rAddA) + 1);
									vaRamCount 	:= vaRamCount + 1;								
									vaState   	:= STATE_ECHO_HOLD;
								else
									vaCount := vaCount + 1;
								end if;
							end if;
							
						when STATE_READ_RAM =>
							--saDebug(3 downto 0) <= "1011";
							rEnA	<= '1';
							rWrA	<= '0';
							if vaCount = 0 then
								rAddA 	<=	(others => '0');
							elsif vaCount = 1 then
								--get RAM byte
								rClkA				<= '1';
							elsif vaCount = 2 then
								--send data to Host
								saHostTxD		<= rDataOutA;
								sWDReady  	<= '1';								
							end if;
							
							if sWR = '1' then
								--read accepted so send next byte
								sWDReady  	<= '0';
								rClkA				<= '0';
								rAddA				<= rAddA + 1;
								vaCount 		:= (others => '0');
								vaCount(0) 	:= '1';
							else
								vaCount 		:= vaCount + 1;		
							end if;
							
							if (rAddA = vaRamCount) then 
								-- all RAM read
								vaState   := STATE_IDLE;	
							end if;
				
						when STATE_READ_DDS =>
							--read back the contents of buffer holding DDS read data
							if vaInBufferFull = '1' then
								if vaCount(0) = '0' then
									saHostTxD		<=	STD_LOGIC_VECTOR(vaInBuffer(7 downto 0));
									sWDReady		<=	'1';
								else
									sWDReady		<=	'0';
									vaInBuffer	:=	vaInBuffer srl 8;
									if vaCount = 15 then
										vaInBufferFull 	:= '0';
										vaState   			:= STATE_REPLY_WAIT;
									end if;
								end if;
								vaCount		:=	vaCount + 1;			
							end if;

						when STATE_TOGGLE_SWEEP =>
							--toggle the trigger on, waiting for a trigger to perform a sweeping w.f
							if vaSweepOn = '0' and vaRamCount = 0 then
								vaCopyTrigger 				:= '1';		
								vaSweepOn			 				:= '1';
								vaTriggerLive	 				:= '1';
								rAddB									<=  (others => '0');
							elsif sRD = '1' and vaCopyTrigger = '0' then
								--stay in state until another 0x2D
								if saD = "00101101" then
									saHostTxD							<= "00101101";
									sWDReady  						<= '1';						
									vaState								:= STATE_REPLY_WAIT;
								end if;
							end if;
							
							sBusy 				<= '0';								
						
						
						when STATE_WRITE_TRIG =>								
							--get new byte and send to trig RAM
							tWE						<=	'1';
							
							if vaCount = 0 then
								--if saD(4) then in sweep mode (this changes the format of the data that must be read from the PC)	
								vaIsSweep 			:=	saD(4);
								
								--get first byte
								sBusy 				<= 	'0';
								vaCount				:= 	vaCount + 1;
							elsif vaCount = 1 and sBusy = '1' then
								vaCount				:= 	vaCount + 1;	
							elsif vaCount = 2 then													
								
								if vaIsSweep = '1' then
									--contains info on length of wait (sweep) 
									vaWaitNeeded(7 downto 0)  := saD;
									
									--next byte also contains wait info	(sweep)							
									vaCount		:= 	vaCount + 1;		

								else
									--first byte gives bytes to send in trigger	(normal trigger)						
									vaTrigBytes(7 downto 0) :=  saD;
									
									tClk <= '0';
									
									--go straight to data transfer (normal trigger)
									vaCount := (3 => '1', others => '0'); --vaCount = 8
								end if;
								
								--get next byte
								sBusy 			<= 	'0';
							
							elsif sBusy = '1' then
								if vaCount = 3 then
									--final part of wait data (sweep)
									vaWaitNeeded(15 downto 8)	:= saD;
									sBusy											<= '0';
									tClk											<= '0';
								elsif vaCount = 4 then
									--first part of length data (sweep)
									vaTrigBytes(7 downto 0)  	:= saD;
									tIn 											<= saD;
								elsif vaCount = 5 then
									tClk											<= '1';
								elsif vaCount = 6 then
									tAdd 		<= 	tAdd + 1;
									tClk		<= 	'0';
									sBusy 	<=	'0';
								elsif vaCount = 7 then
									--final part of length data (sweep)
									vaTrigBytes(10 downto 8) := saD(2 downto 0);
								elsif vaCount = 9 then
									--get data bytes
									tIn 		<=	saD;		
								elsif vaCount = 10 then
									tClk	<= '1';
								elsif vaCount = 11 then
									tAdd 		<= 	tAdd + 1;
									tClk		<= 	'0';
									sBusy 	<=	'0';
									vaCount := (3 => '1', others => '0'); --vaCount = 8
									
									--done
									if tAdd = vaTrigBytes then
										saHostTxD 	<= tAdd(7 downto 0);
										sWDReady		<=	'1';
										vaTrigBytes := (others => '0');
										vaState			:= STATE_REPLY_WAIT;
									end if;
								end if;
								vaCount		:= 	vaCount + 1;						
							end if;
							
							--host sends byte
							if sRD = '1' then
								sBusy 	<= '1';		
							end if;
							
						when STATE_READ_TRIG =>
							--read the contents of the trigger RAM
							tWE			<=	'0';
							
							if vaCount = 1 then
								tClk 			<=	'1';
							elsif vaCount = 2 then
								saHostTxD							<= tOut;
								sWDReady  						<= '1';	
							end if;
							
							if sWR = '1' then
								sWDReady 		<= '0';
								tClk 				<= '0';
								tAdd				<= tAdd + 1;
								vaCount 		:= (0 => '1', others => '0');
							else
								vaCount 		:= vaCount + 1;
							end if;
							
							if tAdd = "001001" then
								vaState			:= STATE_IDLE;								
							end if;
							
						when STATE_SET_PROFILE =>
							--set the profile pins on the DDS
							sProfile 								<= saD(2 downto 0);
							saHostTxD(2 downto 0)		<= saD(2 downto 0);
							saHostTxD(7 downto 3)		<= (others => '0');
              sWDReady  							<= '1';							
							vaState									:= STATE_REPLY_WAIT;
						
						when STATE_TRIG_WAIT =>					
							--go into a state waiting for an external trigger before updating DDS
							if vaTriggerLive = '0' and vaRamCount = 0 then
								vaCopyTrigger 				:= '1';		
								vaTriggerLive 				:= '1';
								vaSweepOn			 				:= '0';
								rAddB									<=  (others => '0');
							elsif sRD = '1' and vaCopyTrigger = '0' then
								--stay in state until another 0x2A
								if saD = "00101010" then
									saHostTxD							<= "00101010";
									sWDReady  						<= '1';						
									vaState								:= STATE_REPLY_WAIT;
								end if;
							end if;
							
							sBusy 				<= '0';										
						
            when STATE_REPLY_WAIT =>
              --saDebug(3 downto 0) <= "1100";
              if sWR = '1' then
                sWDReady <= '0';
                vaState  := STATE_IDLE;
              end if;
						
            when others =>
              --saDebug(3 downto 0) <= "1101";
							--vaState  		:= STATE_IDLE;
              --sWDReady   <= '0';
							saHostTxD		<= saD;
							vaState  		:= STATE_REPLY_WAIT;
							sWDReady   	<= '1';
              sBusy      	<= '0';
              sWRegReady 	<= '0';
              
          end case;
	
			-----------------------------------------------------------------------------
			-- Transfer Protocol
			-----------------------------------------------------------------------------

					--send RAM down sSDIO 
					--when sweep trigger wait for the DDS to be updated before sending the next 12
					if (vaRAMCount > 0 and not (vaRampBytesSent > 13 and vaSweepOn = '1')) and vaUpdateNeeded = '0' then
						rEnB			<=	'1';
						rWrB			<= 	'0';
						--get RAM
						if rClkB = '1' then
							--check for instruction
							if vaNumBytesOut = 0 then
								--check if in buffer is full, only send read command if empty
								if not (rDataOutB(7) = '1' and vaInBufferFull = '1') then
									if rDataOutB(6 downto 5) = "11" then
										vaNumBytesOut 	:=	"0000000001001";
									elsif rDataOutB(6 downto 5) = "00" then
										--this means the DDS RAM is being set so 4096 bytes are expected
										vaNumBytesOut 	:=	"1000000000001";
									else
										vaNumBytesOut(11 downto 3) := (others => '0');
										vaNumBytesOut(2 downto 1)	:=	rDataOutB(6 downto 5) + 1;
										vaNumBytesOut(0) 					:= 	'1';
									end if;
									vaReadInstr							:=	rDataOutB(7);
									vaOutByte(4 downto 0)		:=	unsigned(rDataOutB(4 downto 0));
									vaOutByte(7)						:=	rDataOutB(7);
								end if;
							elsif vaSend = 0 then
								--get RAM byte
								vaOutByte(6 downto 0)	:=	unsigned(rDataOutB(6 downto 0));
								vaOutByte(7)					:=	rDataOutB(7);
							end if;		
							--send one byte to device
							if vaSend < 34 then
								if vaSend(1) = '0' then
									sSDOut				<=	vaOutByte(7);
								else
									if vaSend(0) = '1' then
										vaOutByte			:=	vaOutByte sll 1;
									end if;
									if vaReadInstr = '1' then
										if vaSend(0) = '0' then
											vaInBuffer		:= 	vaInBuffer sll 1;
										else
											vaInBuffer(0)	:=	sSDIn;
										end if;
									end if;
								end if;
								
								sSDC				<=	vaSend(1);
								vaSend			:=	vaSend + 1;
							else
								--byte sent
								vaSend 			:= 	(others => '0');
								rClkB				<=	'0';						
								vaRAMCount 	:= 	vaRAMCount - 1;
								rAddB				<= 	rAddB + 1;
								vaNumBytesOut	:=	vaNumBytesOut - 1;
								if vaSweepOn = '1' then
									--count number of ramp bytes sent
									vaRampBytesSent	:=	vaRampBytesSent + 1;
									if vaRAMCount = 0 then
										--end sweep
										vaTriggerLive := '1';
										vaCopyTrigger := '1';
										rAddB					<= (others => '0');
									end if;
								end if;
								if vaNumBytesOut = 0 then
									--all register sent
									if vaReadInstr = '1' then
										--in buffer is full
										vaInBufferFull 	:= '1';
										sReadSDIO 			<= '0';
									elsif vaTriggerLive = '0' and vaSweepOn = '0' then
										--register updated
										vaUpdateNeeded 	:=	'1';
									end if;
								else
									sReadSDIO 	<=	vaReadInstr;								
								end if;
							end if;
						else					
							rClkB		<=	'1';	
						end if;
					end if;
					
			-----------------------------------------------------------------------------
			-- Trigger Protocol
			-----------------------------------------------------------------------------

					if vaCopyTrigger = '1' then
						--copy next profile of trigger RAM into transmision RAM
						rEnA 			<= '1';
						rWrA			<= '1';
						tWE				<= '0';
						--tProf 		<= (others => '0');
						
						if vaCopyCount = 0 then
							tAdd 				<= (others => '0');
							rAddA 			<= (others => '0');
							rClkA				<=	'0';
							tClk				<=	'0';
						elsif vaCopyCount = 1 then
							tClk 			<= '1';
						elsif vaCopyCount = 2 then
							if vaSweepOn = '1' then
								--get number of bytes to copy from first byte
								vaTrigBytes(7 downto 0) := tOut;
						
								--more length data
								vaCopyCount(3 downto 0) := "0011"; --vaCopyCount = 3
							else							
								if tOut = 0 then
									--no trigger to copy
									vaCopyCount(3 downto 0)	:=	"1100"; --vaCopyCount = 12
								else
									--get number of bytes to copy from first byte
									vaTrigBytes(7 downto 0) := tOut + 1;

									vaCopyCount(3 downto 0)	:=	"1000"; --vaCopyCount = 8
								end if;
							end if;
							
						--SweepOn Data
						----------------------------
						elsif vaCopyCount = 4 then
								tAdd		<= 	tAdd + 1;		
								tClk		<= '0';
						elsif vaCopyCount = 5 then
								tClk		<= '1';
						elsif vaCopyCount = 6 then
								--get rest of bytes to send
								vaTrigBytes(10 downto 8) := tOut(2 downto 0);
						elsif vaCopyCount = 7 then
								tAdd		<= 	tAdd + 1;
						----------------------------
						
						elsif vaCopyCount = 8 then
							tClk 			<= '0';
						elsif vaCopyCount = 9 then
							tClk			<= '1';
						elsif vaCopyCount = 10 then
							rDataInA 	<=	tOut;
						elsif vaCopyCount = 11 then
							rClkA			<= '1';
						elsif vaCopyCount = 12 then
							tClk						<= '0';
							rClkA						<= '0';
							tAdd						<= 	tAdd + 1;							
							rAddA						<= 	rAddA + 1;
							vaRamCount  		:=  vaRamCount + 1;
						elsif vaCopyCount = 13 then
							--copy next byte
							vaCopyCount	:=	( 3 => '1', others => '0'); --vaCopyCount = 8
						end if;
						
						if (vaCopyCount < 14) and (not (vaCopyCount = 2)) then
							vaCopyCount := vaCopyCount + 1;
						end if;
						
						if tAdd = vaTrigBytes + 1 and vaCopyCount = 12 then
							--done copying
							vaCopyTrigger := '0';
							vaCopyCount   := (others => '0');
							rEnA					<= '0';
							tClk					<= '0';
							rClkA					<= '0';
						end if;
						
					end if;
					
					--update DDS at specific times when in sweep mode
					if vaSweepOn = '1'	and vaTriggerLive = '0' then
						if vaRampBytesSent	= 14 and vaWaitTemp = vaWaitNeeded then
							vaRampBytesSent	:= (others => '0');
							vaUpdateNeeded 	:=	'1';
							vaWaitTemp			:= (others => '0');
						else
							vaWaitTemp			:= vaWaitTemp + 1;
						end if;
					end if;
					
					--receipt of trigger pulse
					if vaTriggerLive = '1' then
						--deal with sweep starting
						if sTrigger = '1' and vaSweepOn = '1' then
							vaTriggerLive := '0';
							vaUpdateNeeded 	:=	'1';
						else					
							--change profile
							sProfile(0) 	<= 	sTrigger; 
							sProfile(2 downto 1) <= (others => '0'); 
						end if;
					end if;					
					
					--send update pulse (needs to be a few clocks long) 
					if vaUpdateNeeded = '1' then
						if vaUpCount > 10 then
							sIOup		<=	'1';
							if vaUpCount = 25 then
								vaUpdateNeeded 	:= '0';
								vaUpCount 			:= (others => '0');
							end if;
						end if;
						vaUpCount 			:= vaUpCount + 1;
					else
						sIOup		<=	'0';
					end if;
					
					
    -----------------------------------------------------------------------------
    -- Debugging
    -----------------------------------------------------------------------------
					if sProfile(0) = '1' then				
						saTIO(13)							<= '1';
					else
						saTIO(13)							<= '0';
					end if;

					saTIO(14)							<= vaSweepOn and sIOup;
					saTIO(15)							<= vaSweepOn;
					saTIO(16)							<= vaTriggerLive;
					saTIO(17)							<= vaCopyTrigger;
					saTIO(18)							<= vaSweepOn and saTIO(0);
					saTIO(19) 						<= vaSweepOn and saTIO(1);				
					sIOres 								<= '0';
					
				end process;
			
    -----------------------------------------------------------------------------
    -- Temporary signal parking
    -----------------------------------------------------------------------------
      
--      saTOE(11) <= '1';
--      saTIO(11) <= saDebug(0);
--      saTOE(10) <= '1';
--      saTIO(10) <= saDebug(1);
--      saTOE(9)  <= '1';
--      saTIO(9)  <= saDebug(2);
--      saTOE(8)  <= '1';
--      saTIO(8)  <= saDebug(3);
--      saTOE(7)  <= '1';
--      saTIO(7)  <= saDebug(4);
--      saTOE(6)  <= '1';
--      saTIO(6)  <= saDebug(5);
--      saTOE(5)  <= '1';
--      saTIO(5)  <= saDebug(6);
--      saTOE(4)  <= '1';
--      saTIO(4)  <= saDebug(7);
      
			--sSDC
      saTIO(0)   <= sSDC;
      saTOE(0)   <= '1';
			
			--sSDIO
			saTIO(1) 	<=	sSDOut when sReadSDIO = '0' else 'Z';
			sSDIn			<=	saTIO(1) when sReadSDIO = '1' else '0';
			saTOE(1)	<=	not sReadSDIO;
			
			--sIOres
			saTIO(2) 	<=	'0' when sIOres = '0' else 'Z';
			saTOE(2)	<=	'1';
			
			--sIOup
			saTIO(4) 	<=  sIOup;
			saTOE(4)	<=	'1';
      
			--sProfile
			saTIO(5)  <= sProfile(0);			
      saTIO(6)  <= sProfile(1);
      saTIO(7)  <= sProfile(2);
      saTOE(5)  <= '1';
      saTOE(6)  <= '1';
      saTOE(7)  <= '1';
			
			--sDRctrl
			saTIO(12)	<=	sDRctrl;
			saTOE(12)  <= '1';
			
			
			--sTrigger
			saTIO9		<= 'Z';
			sTrigger 	<= '1' when paTIO9 = '1' else '0';
			saTIO(8) 	<= '1' when paTIO9 = '1' else '0';
			saTOE(8) 	<= '1';
			saTOE(9) 	<= '0';
			
      sINIT   <= '0';
      sSI_WU  <= '0';
      sSDO    <= '0';
      sSCK    <= '0';
      sCSADC  <= '0';
      sCSV1   <= '0';
      sCSV2   <= '0';
      s15V_EN <= '0';
      --saTOE(0)  <= '1';
      --saTOE(1)  <= '1';
      --saTOE(2)  <= '1';
      --saTOE(3)  <= '1';
      --saTOE(4)  <= '1';
      saTOE(10) <= '1';
      saTOE(11) <= '1';
      saTOE(13) <= '1';			
			saTOE(14)  <= '1';
			saTOE(15)  <= '1';
      saTOE(16)  <= '1';
      saTOE(17) <= '1';
      saTOE(18) <= '1';			
      --saTOE(18) <= '0';
      saTOE(19) <= '1';
      --saTOE(20) <= '0';
      --saTOE(22) <= '0';

      --saTIO(0)  <= sMCLR;
      --saTIO(1)  <= saDebug(1);
      --saTIO(2)  <= saDebug(2);
      --saTIO(3)  <= saDebug(3);
      --saTIO(4)  <= saDebug(4);

      --saTIO(8)  <= saDebug(8);
      --saTIO(9)  <= saDebug(9);
      --saTIO(10)  <= saDebug(10);
      --saTIO(11)  <= saDebug(11);			
      --saTIO(1)  <= 'Z';
      --saTIO(2)  <= 'Z';
      --saTIO(3)  <= 'Z';
      --saTIO(4)  <= 'Z';
      --saTIO(5)  <= 'Z';
      --saTIO(6)  <= 'Z';
      --saTIO(7)  <= 'Z';
      --saTIO(8)  <= 'Z';
      --saTIO(9)  <= 'Z';
      --saTIO(10) <= 'Z';
      --saTIO(11) <= 'Z';
      --saTIO(12) <= 'Z';
      --saTIO(13) <= 'Z';
      --saTIO(14) <= 'Z';
      --saTIO(15) <= 'Z';
      --saTIO(16) <= 'Z';
      --saTIO(17) <= 'Z';
      --saTIO(18) <= 'Z';
      --saTIO(19) <= 'Z';
      --saTIO(20) <= 'Z';
      --saTIO(21) <= 'Z';
      
      
  end Behavioral;

