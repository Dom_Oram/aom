#include <windows.h>
#include <string>
#include "ftd2xx.h"
using namespace std;

#ifndef FPGA_H
#define FPGA_H
	FT_STATUS setUpForFPGA();
	void closeFPGA();
	bool isFPGAProg();
	void setFPGADebugPrint(bool debug);
	FT_STATUS writeSVF(string filename);
	FT_STATUS setUpMPSSE();
#endif