#include <string>
#include <math.h>
#include "ftd2xx.h"
using namespace std;

class Scan{
	///
	///	This class will parse a line from an svf file to create an output that can be sent to the xilinx
	///

	bool isIR;
	int readBytesLen;
	int len;
	string writeData;
	unsigned char writeMask[65535];
	unsigned char expectedReadData[65535];
	unsigned char expectedReadMask[65535];

	string getNextWord(string line, size_t start)
	{
		string word;
		line = line.substr(start, line.length());

		if ( line.find_first_of(" ") != -1) word.append(line.substr(line.find_first_of(" ") + 1, line.length()));
		else cout << "Parsing error";

		if ( word.find_first_of(" ") != -1) word = word.substr(0, word.find_first_of(" "));

		return word;
	}

	string getBracketData(string line, size_t start)
	{
		string data = "";
		line = line.substr(start, line.length() - start);
	
		if ( line.find_first_of("(") != -1) data.append(line.substr(line.find_first_of("(") + 1, line.length()));
		else cout << "Parsing error";

		if ( data.find_first_of(")") != -1) data = data.substr(0, data.find_first_of(")"));
		else cout << "Parsing error";
	
		return data;
	}

	void getDataFromString(string line)
    {
        size_t atChar = 3;	
		readBytesLen = 0;

        //get length
        string strValue = getNextWord(line, atChar);
		len = atoi(strValue.c_str());
		atChar += strValue.length() + 2; //2 for spaces either side

        
		if(len != 0){

            //get write data
            writeData = getBracketData(line, atChar);
            atChar += writeData.length() + 6; //+6 as TDI + space + 2 brackets 

			if(getNextWord(line, atChar) == "SMASK"){
                //get write mask
				string wMask = getBracketData(line, atChar);
                atChar += wMask.length() + 9;//+9 as space + SMASK + space + 2 brackets
                
				//convert string
				for (int i = 1; i <= readBytesLen; i++){
					int byte;
					sscanf_s(wMask.substr(2 * (readBytesLen - i) , 2).c_str(), "%2X", &byte);
					writeMask[i - 1] = byte; //convert integer to BYTE and add to list
				}
            }

            
			if(getNextWord(line, atChar) == "TDO"){
		
				readBytesLen = (int)ceil(len/8.0);

				//get read data
                string rData = getBracketData(line, atChar);		
					
				//convert string
				for (int i = 1; i <= readBytesLen; i++){
					int byte;
					sscanf_s(rData.substr(2 * (readBytesLen - i) , 2).c_str(), "%2X", &byte);
					expectedReadData[i - 1] = byte; //convert integer to BYTE and add to list
				}

                atChar += 7 + rData.length();//+7 as space + TDO + space + ()

                if(getNextWord(line, atChar) == "MASK"){
                    //get read mask
					string rMask = getBracketData(line, atChar);
					
					//convert string
					for (int i = 1; i <= readBytesLen; i++){
						int byte;
						sscanf_s(rMask.substr(2 * (readBytesLen - i) , 2).c_str(), "%2X", &byte);
						expectedReadMask[i - 1] = byte; //convert integer to BYTE and add to list
					}
					
					//apply read mask
					for (int i = 0; i < readBytesLen; i++){
						expectedReadData[i] &= expectedReadMask[i];
					}
				}else{
					for (int i = 0; i < readBytesLen; i++){
						expectedReadMask[i] = 0xff;
					}
				}
            }

        }
    }

public:
	Scan(){
		len = 0;
		writeData = "";
		readBytesLen = 0;
	}

	void setData(string line, bool IR)
	{
		isIR = IR;
		getDataFromString(line);
	}
	int getLen() {return len;}

	string getwBuffer(){	return writeData;}

	int getReadLen(){ return readBytesLen;}

	bool IR(){return isIR;}

	bool checkReadBuffer(unsigned char readBuffer[])
	{
		for (int i = 0; i < readBytesLen; i++){
			readBuffer[i] &= expectedReadMask[i];
			if(readBuffer[i] != expectedReadData[i]){
				cout << "Actual read value: " << hex << (int) readBuffer[i] << " Expected read value: " << (int)expectedReadData[i] << "\n";
				return false;
			}
		}

		return true;
	}
};