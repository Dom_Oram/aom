#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <math.h>
#include "FPGA.h"
#include "cl_Scan.h"
#include "Parsing.h"
using namespace std;

static FT_HANDLE ftHandle;

static unsigned char ReadBuffer[65535];
static unsigned char WriteBuffer[65535];

ifstream FPGA_SVF;

//read and write buffers for headers, trailers and data
static Scan I_Trailer;
static Scan D_Trailer;
static Scan I_Data;
static Scan D_Data;
static Scan I_Header;
static Scan D_Header;

int writeNum = 0;
int readNum = 0;

bool printDebug = 0;

static FT_STATUS ftStatus = 0;

enum {
	IDLE_STATE,
	RESET_STATE,
	PAUSE_D_REG_STATE,
	SHIFT_D_REG_STATE,
	CAP_D_REG_STATE,
	PAUSE_I_REG_STATE,	
	SHIFT_I_REG_STATE,
	CAP_I_REG_STATE,
};

DWORD deafultIR;
DWORD deafultDR;

DWORD currentState = IDLE_STATE;
static DWORD NumBytesToSend = 0;
static DWORD NumBytesSent = 0;

/*
TRST	(Test ReSeT) Controls the optional Test Reset line.
TDR	(Trailer Data Register) Specifies a trailer pattern that is appended to the end of subsequent DR scan operations.
TIR	(Trailer Instruction Register) Specifies a trailer pattern that is appended to the end of subsequent IR scan operations.
SDR	(Scan Data Register) Performs an IEEE 1149.1 Data Register scan.
SIR	(Scan Instruction Register) Performs an IEEE 1149.1 Instruction Register scan.
HDR	(Header Data Register) Specifies a header pattern that is prepended to the beginning of subsequent DR scan operations.
HIR	(Header Instruction Register) Specifies a header pattern that is prepended to the beginning of subsequent IR scan operations.
ENDIR	Specifies default end state for IR scan operations.
ENDDR	Specifies default end state for DR scan operations.
RUNTEST	Forces the IEEE 1149.1 bus to a run state for a specified number of clocks or a specified time period.
STATE	Forces the IEEE 1149.1 bus to a specified stable state.
PIO	(Parallel Input/Output) Specifies a parallel test pattern.
PIOMAP	(Parallel Input/Output Map) Maps PIO column positions to a logical pin.
FREQUENCY	Specifies maximum test clock frequency for IEEE 1149.1 bus operations.
*/
DWORD readChip(FT_HANDLE ftHandle)
{
	DWORD NumBytesToRead = 0; // Number of bytes available to read in the driver's input buffer
	DWORD NumBytesRead = 0; // Count of actual bytes read - used with FT_Read
	do
	{
		// Get the number of bytes in the device input buffer
		ftStatus = FT_GetQueueStatus(ftHandle, &NumBytesToRead);

	} while ((NumBytesToRead == 0) && (ftStatus == FT_OK));

	ftStatus = FT_Read(ftHandle, &ReadBuffer, NumBytesToRead, &NumBytesRead); 
	if(printDebug){
		for (unsigned int i = 0; i < NumBytesToRead; i++)
			cout << hex << (int)ReadBuffer[i] << " ";
		cout << "\n";
	}

	return NumBytesRead;
}

void setFPGADebugPrint(bool debug)
{
	printDebug = debug;
}

bool testMPSSESync()
{	
	///
	/// Tests and syncs the MPSSE function of the FT2232 by sending bogus data and waiting for echo, returns true if chip OK
	///
	DWORD unused;

	//Add bogus command �xAA� and 'xAB' to the queue and send
	WriteBuffer[NumBytesToSend++] = 0xAA;//'\xAA';
	WriteBuffer[NumBytesToSend++] = 0xAB;//'\xAB';

	FT_Write(ftHandle, WriteBuffer, NumBytesToSend, &unused);
	NumBytesToSend = 0;

	//Read out the data from input buffer and check for bad and echo
	bool bCommandEchod = false;

	int ReadBytes = readChip(ftHandle);
	for (int i = 0; i < ReadBytes; i++){
		if ((ReadBuffer[i] == 0xFA) && (ReadBuffer[i + 1] == 0xAA)) 
			if ((ReadBuffer[i + 2] == 0xFA) && (ReadBuffer[i + 3] == 0xAB))bCommandEchod = true;
	}
	return bCommandEchod;
}


FT_STATUS setUpForFPGA()
{
	///
	///Sets up the B bus on the FT2232 in MPSSE mode to program Xlinx
	///
	DWORD NumBytesToRead;

	//initialize chip for MPSSE and JTAG, returns if error
	ftStatus |= FT_Open(1, &ftHandle);

	//reset device
	ftStatus |= FT_ResetDevice(ftHandle);

	//Purge USB receive buffer first by reading out all old data from FT2232H receive buffer
	ftStatus |= FT_GetQueueStatus(ftHandle, &NumBytesToRead); 

	// Get the number of bytes in the FT2232H receive buffer and read
	if ((ftStatus == FT_OK) && (NumBytesToRead > 0)) FT_Read(ftHandle, &ReadBuffer, NumBytesToRead, NULL);
	
	//Set USB request transfer sizes to 64K
	ftStatus |= FT_SetUSBParameters(ftHandle, 65536, 65535); 
	
	//Disable event and error characters
	ftStatus |= FT_SetChars(ftHandle, false, 0, false, 0); 

	//Sets the read and write timeouts in milliseconds 
	ftStatus |= FT_SetTimeouts(ftHandle, 0, 5000); 

	//set control flow
	ftStatus |= FT_SetFlowControl(ftHandle, FT_FLOW_RTS_CTS, 0x00, 0x00);

	//Set the latency timer (default is 16mS)
	ftStatus |= FT_SetLatencyTimer(ftHandle, 16);

	//Reset controller
	//ftStatus |= FT_SetBitMode(ftHandle, 0x0, 0x00); 

	return ftStatus;

}
FT_STATUS setUpMPSSE()
{
	///
	///Enables and configures the FT to MPSSE mode then tests
	///

	ftStatus |= FT_SetBitMode(ftHandle, 0x1B, 0x02);

	if (testMPSSESync()) cout << "MPSSE Synced\n";
	
	// Use 60MHz master clock (disable divide by 5)
	WriteBuffer[NumBytesToSend++] = 0x8A;

	// Turn off adaptive clocking (may be needed for ARM)
	WriteBuffer[NumBytesToSend++] = 0x97;
	
	// Disable three-phase clocking
	WriteBuffer[NumBytesToSend++] = 0x8D;

	// Set initial states of the MPSSE interface - low byte, both pin directions and output values
	// ADBUS0 TCK output low 
	// ADBUS1 TDI output low
	// ADBUS2 TDO input 
	// ADBUS3 TMS output high
	// ADBUS4 PROG output low
	// ADBUS5 DONE input 
	// ADBUS6 /INIT input
	// ADBUS7 READY input

	// Set prog low-high-low
	// Low
	// Set data bits low-byte of MPSSE port	
	WriteBuffer[NumBytesToSend++] = 0x80;

	// Initial state config above
	WriteBuffer[NumBytesToSend++] = 0x08;

	// Direction config above
	WriteBuffer[NumBytesToSend++] = 0x1B;

	// High
	// Set data bits low-byte of MPSSE port	
	WriteBuffer[NumBytesToSend++] = 0x80;

	// Initial state with prog high
	WriteBuffer[NumBytesToSend++] = 0x18;

	// Direction config above
	WriteBuffer[NumBytesToSend++] = 0x1B;

	// Low
	// Set data bits low-byte of MPSSE port	
	WriteBuffer[NumBytesToSend++] = 0x80;

	// Initial state config above
	WriteBuffer[NumBytesToSend++] = 0x08;

	// Direction config above
	WriteBuffer[NumBytesToSend++] = 0x1B;

	// Disable internal loop-back
	WriteBuffer[NumBytesToSend++] = 0x85;
	
	ftStatus |= FT_Write(ftHandle, WriteBuffer, NumBytesToSend, &NumBytesSent);
	
	if(printDebug){
		for (unsigned int i = 0; i < NumBytesToSend; i++)
			cout << hex << (int)WriteBuffer[i] << " ";
		cout << "\n";
	}
	NumBytesToSend = 0;
	
	//Read lower byte
	Sleep (2);	
	WriteBuffer[NumBytesToSend++] = 0x81;
	ftStatus |= FT_Write(ftHandle, WriteBuffer, NumBytesToSend, &NumBytesSent);
	NumBytesToSend = 0;

	//test /INIT high, ready for program
	UCHAR ucData;
	bool init;
	DWORD unused;

	ftStatus |= FT_Read(ftHandle, &ucData, 1, &unused);
	init = !!(ucData & (1 << 6));
	if (!init){
		ftStatus = 1;
		cout << "Init not high\n";
	}
	
	return ftStatus;
}
static bool openSVF(string filename)
{
	///
	///Opens a svf file to write to chip
	///

	FPGA_SVF.open (filename.c_str());
	return FPGA_SVF.fail();
}

void goToState(DWORD State)
{
	///
	/// Changes the JTAG state machine
	///

	if (currentState != State){

		WriteBuffer[NumBytesToSend++] = 0x4B; //TMS no read, will be used regardless of position in TAP
		
		switch (State){

			case RESET_STATE:
				WriteBuffer[NumBytesToSend++] = 0x04; //5 clock pulses
				WriteBuffer[NumBytesToSend++] = 0x1F; //11111
				currentState = RESET_STATE;
				break;

			case IDLE_STATE:
				WriteBuffer[NumBytesToSend++] = 0x05; //6 clock pulses
				WriteBuffer[NumBytesToSend++] = 0x0F; //011111 (LSB)=> 111110
				currentState = IDLE_STATE;
				break;

			case SHIFT_D_REG_STATE:
				switch(currentState){
			
					case IDLE_STATE:
						WriteBuffer[NumBytesToSend++] = 0x02; //3 clock pulses
						WriteBuffer[NumBytesToSend++] = 0x01; //001 (LSB)=> 100
						break;
			
					case RESET_STATE:
						WriteBuffer[NumBytesToSend++] = 0x03; //4 clock pulses
						WriteBuffer[NumBytesToSend++] = 0x02; //0010 (LSB)=> 0100
						break;

					default:
						//go to reset
						goToState(RESET_STATE);
						//go to DR
						WriteBuffer[NumBytesToSend++] = 0x4B; //TMS no read
						WriteBuffer[NumBytesToSend++] = 0x03; //4 clock pulses
						WriteBuffer[NumBytesToSend++] = 0x02; //0010 (LSB)=> 0100

				}
				currentState = SHIFT_D_REG_STATE;
				break;

			case SHIFT_I_REG_STATE:
				switch(currentState){
			
					case IDLE_STATE:
						WriteBuffer[NumBytesToSend++] = 0x03; //4 clock pulses
						WriteBuffer[NumBytesToSend++] = 0x03; //0011 (LSB)=> 1100
						break;
			
					case RESET_STATE:
						WriteBuffer[NumBytesToSend++] = 0x04; //5 clock pulses
						WriteBuffer[NumBytesToSend++] = 0x06; //00110 (LSB)=> 01100
						break;

					default:
						//go to reset
						goToState(RESET_STATE);
						//go to IR
						WriteBuffer[NumBytesToSend++] = 0x4B; //TMS no read
						WriteBuffer[NumBytesToSend++] = 0x04; //5 clock pulses
						WriteBuffer[NumBytesToSend++] = 0x06; //00110 (LSB)=> 01100	
						break;
				}
				currentState = SHIFT_I_REG_STATE;
				break;
		}

		//write data
		ftStatus |= FT_Write(ftHandle, &WriteBuffer, NumBytesToSend, &NumBytesSent);
		NumBytesToSend = 0;
	}
}
bool writeDataToBuffer(Scan scan, bool TMSNext)
{
	///
	///write WriteBuffer to chip, scan contains data to write, TMSNext is true if the data will be followed by a TAP change. 
	///function returns the last bit of the written data
	///

	BYTE writeData[65535];
	string scanBuffer = scan.getwBuffer(); //get data from scan object
	bool lastBit = FALSE;

	//go to shift-IR/DR
	if (scan.IR()) goToState(SHIFT_I_REG_STATE);
	else goToState(SHIFT_D_REG_STATE);

	//get read length
	int expectedReadBytes = scan.getReadLen();

	//get length in bytes and bits
	int bitLen = scan.getLen();
	int bytesLen = (int)ceil(bitLen/8.0);

	//if data is too large send 64Kb first
	while (bytesLen >= 64000){
	
		if (TMSNext && bytesLen == 64000){
			bitLen--; //as last bit will be clocked by TMS change
			lastBit = !!(writeData[bytesLen-1] & (1 << (bitLen % 8))); //get last bit from write buffer
		}

		for (int i = 1; i <= 64000; i++){
			int byte;
			sscanf_s(scanBuffer.substr(2 * (bytesLen - i) , 2).c_str(), "%2X", &byte); //get string of LSB byte in data and convert to integer
			writeData[i - 1] = byte; //convert integer to BYTE and add to list
		}

		//send bytes
		if (expectedReadBytes != 0) WriteBuffer[NumBytesToSend++] = 0x39; //clock out bytes and read
		else WriteBuffer[NumBytesToSend++] = 0x19; //clock out bytes, dont read

		WriteBuffer[NumBytesToSend++] = 0xFF; //clock pulses
		WriteBuffer[NumBytesToSend++] = 0xF9; //clock pulses
		
		for (int i = 0; i < 64000; i++)
			WriteBuffer[NumBytesToSend++] = writeData[i];
		
		//recalculate bitsLen and bytesLen
		bitLen -= 512000;		
		bytesLen -= 64000;

		//send data
		ftStatus |= FT_Write(ftHandle, &WriteBuffer, NumBytesToSend, &NumBytesSent);
		if(ftStatus != FT_OK || NumBytesToSend != NumBytesSent) cout << "Data write failed\n";

		NumBytesToSend = 0;
		Sleep(5); //wait for 64Kb to be sent through
		if(!testMPSSESync()) cout << "Not Synced!\n"; //test still synced
	}
	
	//convert rest of data to correct format
	if (bitLen != 0){
	for (int i = 1; i <= bytesLen; i++){
		int byte;
		sscanf_s(scanBuffer.substr(2 * (bytesLen - i) , 2).c_str(), "%2X", &byte); //get string of LSB byte in data and convert to integer
		writeData[i - 1] = byte; //convert integer to BYTE and add to list
	}

	if (TMSNext){
		bitLen--; //as last bit will be clocked by TMS change
		lastBit = !!(writeData[bytesLen-1] & (1 << (bitLen % 8))); //get last bit from write buffer
	}

	if(bitLen >= 8){
		//convert to hi/lo chars
		unsigned char low_char;
		unsigned char high_char;
		low_char = LOBYTE((int)floor(bitLen/8.0) - 1); //-1 as 0x0000 is 1 clock
		high_char = HIBYTE((int)floor(bitLen/8.0) - 1);	

		//send bytes
		if (expectedReadBytes != 0) WriteBuffer[NumBytesToSend++] = 0x39; //clock out bytes and read
		else WriteBuffer[NumBytesToSend++] = 0x19; //clock out bytes, dont read

		WriteBuffer[NumBytesToSend++] = low_char; //clock pulses
		WriteBuffer[NumBytesToSend++] = high_char; //clock pulses
		
		for (int i = 0; i < (int)floor(bitLen/8.0); i++) //floor as last byte will be clocked as bits if not full
			WriteBuffer[NumBytesToSend++] = writeData[i];
	}
	if((bitLen % 8) > 0){
		//send bits
		if (expectedReadBytes != 0)
			WriteBuffer[NumBytesToSend++] = 0x3B; //clock out bits and read
		else
			WriteBuffer[NumBytesToSend++] = 0x1B; //clock out bits, dont read
		WriteBuffer[NumBytesToSend++] = ((bitLen % 8)  - 1); //clock pulses -1 as 0x00 is one clock
		
		WriteBuffer[NumBytesToSend++] = writeData[bytesLen-1]; //last byte
	}

	}
	return lastBit;
}


void writeDataToChipAndRead(Scan scan, bool lastBit)
{
	//finish writing data and read outcome, scan contains expected read data, lastBit is data to be sent on TMS

	int expectedReadBytes = scan.getReadLen();
	
	//get TMS to write, including data from before
	char data = 0x03;
	if(lastBit) data |= 1 << 7;
	else data &= ~(1 << 7);
	
	//go to idle state via update
	if (expectedReadBytes != 0){

		WriteBuffer[NumBytesToSend++] = 0x6B; //TMS read
		WriteBuffer[NumBytesToSend++] = 0x00; //1 clock pulse
		WriteBuffer[NumBytesToSend++] = data; //1100000(lastBit) (LSB)=> (lastBit)0000011

		//rest of TMS
		WriteBuffer[NumBytesToSend++] = 0x4B; //TMS no read
		WriteBuffer[NumBytesToSend++] = 0x01; //2 clock pulse
		WriteBuffer[NumBytesToSend++] = 0x01; //01 (LSB)=> 10
	}else{
		WriteBuffer[NumBytesToSend++] = 0x4B; //TMS no read
		WriteBuffer[NumBytesToSend++] = 0x02; //3 clock pulse and data
		WriteBuffer[NumBytesToSend++] = data; //01 (LSB)=> (lastBit)10
	}
	currentState = IDLE_STATE;
	
	//write data
	ftStatus |= FT_Write(ftHandle, &WriteBuffer, NumBytesToSend, &NumBytesSent);

	//print written data
	if(printDebug){
		for (unsigned int i = 0; i < NumBytesToSend; i++)
			cout << hex << (int)WriteBuffer[i] << " ";
		cout << "\n";
	}

	if(ftStatus != FT_OK || NumBytesToSend != NumBytesSent) cout << "Data write failed\n";
	
	//check read
	if (expectedReadBytes != 0){
		//get read
		int ReadBytes = readChip(ftHandle);

		//coalesce final bytes as last bit was sent seperately and so read as a new byte
		ReadBuffer[ReadBytes - 2] = ReadBuffer[ReadBytes - 2] >> 1;
		int bit = ReadBuffer[ReadBytes - 1] & (1 << 7);
		if(!!bit) ReadBuffer[ReadBytes - 2] |= 1 << 7;
		else ReadBuffer[ReadBytes - 2] &= ~(1 << 7);
		ReadBytes--;
		
		//shift bits if less than a byte
		int bitLen = scan.getLen();
		if ((bitLen % 8) != 0)
			ReadBuffer[ReadBytes - 1] = ReadBuffer[ReadBytes - 1] >> (8 - (bitLen % 8));	

		if (ReadBytes != expectedReadBytes){
			cout << "Unexpected number of bytes read\n";
			cout << "Actual: " << ReadBytes << " " << "Expected: " << dec << expectedReadBytes << "\n";
		}
		
		//check read buffer
		if(!scan.checkReadBuffer(ReadBuffer)) cout << "Read unexpected\n";
		
		if(printDebug){
			cout << "Write: ";				
			for (unsigned int i = 0; i < NumBytesToSend; i++)				
				cout << hex << (int)WriteBuffer[i] << " ";			
	
			cout << "\nRead: ";			
			for (int i = 0; i < ReadBytes; i++)
				cout << hex << (int)ReadBuffer[i] << " ";
			cout << "\n";
		}
	}

	NumBytesToSend = 0;
}
static void checkLine(string line)
{
	///
	///Parses the svf file
	///

	DWORD unused;

	//get first word
	string command = "";
	if (line != "") command = getNextWord(line, 0);

	if (!command.compare("RUNTEST")){

		//get number of clocks
		string strValue = getNextWord(line, command.length()+1);

		//convert
		int num = 0;
		num = atoi(strValue.c_str());

		//go to IDLE
		goToState(IDLE_STATE);

		if(num >= 8)
		{ 
			//convert num to bytes
			int bytesNum = (int)floor(num/8.0) - 1; //-1 as 0x0000 clocks 8 bytes

			//clock out bytes
			WriteBuffer[NumBytesToSend++] = 0x8F; //clock out no data
			WriteBuffer[NumBytesToSend++] = LOBYTE(bytesNum); //clock pulses (low_char)
			WriteBuffer[NumBytesToSend++] = HIBYTE(bytesNum); //clock pulses (high_char)
		}
		
		if((num % 8) != 0)
		{
			//clock out bits
			WriteBuffer[NumBytesToSend++] = 0x8E; //TMS no read
			WriteBuffer[NumBytesToSend++] = (num % 8) - 1; //clock pulses (-1 as 0x00 clocks 1 bit)
		}

		//write data to chip and print to screen
		ftStatus |= FT_Write(ftHandle, &WriteBuffer, NumBytesToSend, &NumBytesSent);
		
		if(printDebug){
			for (unsigned int i = 0; i < NumBytesToSend; i++)
				cout << hex << (int)WriteBuffer[i] << " ";
			cout << "\n";
		}
		
		if (ftStatus != FT_OK || NumBytesToSend != NumBytesSent) cout << "Run test send error\n";
		NumBytesToSend = 0;

		currentState = IDLE_STATE;
	
	}else if (!command.compare("SDR")){
		//get header, data and trailer write buffers and add to buffer
		writeDataToBuffer(D_Header, FALSE);
		
		D_Data.setData(line, FALSE);
		bool lastBit = writeDataToBuffer(D_Data, TRUE);
		
		int expectedReadBytes = D_Header.getReadLen() + D_Data.getReadLen() + D_Trailer.getReadLen();

		writeDataToChipAndRead(D_Data, lastBit);

	}else if (!command.compare("SIR")){
		//get header, data and trailer write buffers and add to buffer
		writeDataToBuffer(I_Header, FALSE);
		
		I_Data.setData(line, TRUE);
		bool lastBit = writeDataToBuffer(I_Data, TRUE);
		
		int expectedReadBytes = I_Header.getReadLen() + I_Data.getReadLen() + I_Trailer.getReadLen();

		writeDataToChipAndRead(I_Data, lastBit);

	}else if (!command.compare("STATE")){
		//get state	to set to	
		string strValue = getNextWord(line, command.length()+1);

		//set states
		if(strValue == "IDLE")
			goToState(IDLE_STATE);
		
		if (strValue == "RESET")
			goToState(RESET_STATE);

		if((currentState != RESET_STATE) && (currentState != IDLE_STATE)) cout << "State not recognized\n";

	}else if (!command.compare("TDR")){
		//create trailer
		D_Trailer.setData(line, FALSE);

	}else if (!command.compare("TIR")){
		//create trailer
		I_Trailer.setData(line, TRUE);

	}else if (!command.compare("HDR")){
		//create header
		D_Header.setData(line, FALSE);

	}else if (!command.compare("HIR")){
		//create header
		I_Header.setData(line, TRUE);

	}else if (!command.compare("ENDIR")){
		//get end state		
		string strValue = getNextWord(line, command.length()+1);
		
		//set end state
		if(strValue == "IDLE") deafultIR = IDLE_STATE;
		else if (strValue == "IRPAUSE") deafultIR = PAUSE_I_REG_STATE;
		else cout << "End IR state not recognized\n";	

	}else if (!command.compare("ENDDR")){
		//get end state		
		string strValue = getNextWord(line, command.length()+1);
		
		//set end state
		if(strValue == "IDLE") deafultDR = IDLE_STATE;
		else if (strValue == "DRPAUSE") deafultDR = PAUSE_D_REG_STATE;
		else cout << "End DR state not recognized\n";

	}else if (!command.compare("FREQUENCY")){
		//get frequency value
		string strValue = getNextWord(line, command.length()+1);

		//convert and set frequency
		double frequency = atof(strValue.c_str());

		int clockDivisor = (int)(3e7/frequency - 1);

		//Command to set clock divisor
		WriteBuffer[NumBytesToSend++] = 0x86; 
		//Set 0xValueL of clock divisor
		WriteBuffer[NumBytesToSend++] = LOBYTE(clockDivisor);
		//Set 0xValueH of clock divisor
		WriteBuffer[NumBytesToSend++] = HIBYTE(clockDivisor); 
		//Send off the clock divisor commands
		FT_Write(ftHandle, WriteBuffer, NumBytesToSend, &unused);
		
		if(printDebug){
			for (unsigned int i = 0; i < NumBytesToSend; i++)
				cout << hex << (int)WriteBuffer[i] << " ";
			cout << "\n";
		}
		
		NumBytesToSend = 0; // Reset output buffer pointer

	}else if (!command.compare("PIO")){
		//cout << "PIO ";
	}else if (!command.compare("PIOMAP")){
		//cout << "PIOMAP ";
	}else if (!command.compare("TRST")){
		//cout << "TRST ";		
	}
}
void closeFPGA()
{
	///
	/// Safely closes the FT B bus
	///

	//Reset mode
	ftStatus |= FT_SetBitMode(ftHandle, 0x0, 0x00);
	
	//Purge buffers
	ftStatus |= FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX);
	FT_Close(ftHandle);
}

string getLineSVFandCheck()
{
	///
	/// Seperates the lines of the svf and removes comments
	///

	string line;
	
	if (FPGA_SVF.peek() == '/'){ //check if comment
		getline(FPGA_SVF,line);
	}else{
		getline (FPGA_SVF, line, ';');

		//take out new lines
		FPGA_SVF.get(); //take out 1st new line
		line.erase(remove(line.begin(), line.end(), '\n'), line.end()); //remove any others

		//parse
		checkLine(line);

	}
	return line;
}

bool isFPGAProg()
{
	///
	/// Checks DONE pin on Xlinx, if high it has been programmed
	///

	bool prog = 0;
	UCHAR pins = 0x00;

	//Read
	ftStatus |= FT_GetBitMode(ftHandle, &pins);

	if (ftStatus == FT_OK) prog = !!(pins & (1 << 5)); //check
	else cout << "Checking error: " << ftStatus << "\n";
			
	return prog;
}

FT_STATUS writeSVF(string filename)
{
	///
	/// Programs the Xlinx device via FT bus B
	///

	if (openSVF(filename)) cout << "Failed to open file\n";

	int i = 0;
	while (FPGA_SVF.good()){

		string line = getLineSVFandCheck();

		if(printDebug) cout << "L: " << dec << i++ << "\n";

	}

	return ftStatus;
}