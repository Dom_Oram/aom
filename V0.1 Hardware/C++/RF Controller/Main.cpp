#include "FPGA.h"
#include <iostream>
#include "windows.h"
#include "Commdlg.h"
#include "DDS.h"

FT_HANDLE ftHandleFIFO;
FT_HANDLE ftHandleFPGA;

//variables for creating the open file box
OPENFILENAME ofn;
HWND hwnd;
HANDLE hf;


FT_STATUS setUpFT()
{
	///
	///Checks the correct FT device connected and outputs it's name.
	///
	
	FT_STATUS error = 0;
	DWORD NumDevices = 0;
	error = FT_CreateDeviceInfoList(&NumDevices);

	if (NumDevices != 2){
		cout << "Error: Wrong device connected\n";
		cout << "Device may not be powered on\n";
		cout << NumDevices;
		error = 1;
	}

	char DeviceName[100];
	error = FT_GetDeviceInfoDetail(0, NULL, NULL, NULL, NULL, NULL, &DeviceName, &ftHandleFIFO);

	cout << DeviceName << " & ";

	error = FT_GetDeviceInfoDetail(1, NULL, NULL, NULL, NULL, NULL, &DeviceName, &ftHandleFPGA);
	cout << DeviceName << "\n";

	return error;
}

FT_STATUS initFIFO()
{
	///
	/// Initialises bus A to transmit data to the Xilinx via FIFO
	///

	FT_STATUS ftStatus = 0;

	ftStatus |= FT_Open(0, &ftHandleFIFO);

	ftStatus |= FT_SetBitMode(ftHandleFIFO, 0xFF, 0x00);

	ftStatus |= FT_SetLatencyTimer(ftHandleFIFO, 16);

	ftStatus |= FT_SetUSBParameters(ftHandleFIFO, 0x10000, 0x10000);

	ftStatus |= FT_SetFlowControl(ftHandleFIFO, FT_FLOW_RTS_CTS, 0, 0);

	//Purge read buffer and clear internal register
	DWORD RxBytes = 0;
	DWORD unused;

	do{
		ftStatus |= FT_GetStatus(ftHandleFIFO, &RxBytes, &unused, &unused);
		ftStatus |= FT_Read(ftHandleFIFO, &unused, unused, &unused);
	}while(RxBytes != 0);

	if (ftStatus != FT_OK) cout << "Error init FIFO: " << ftStatus << "\n";
	else cout << "FIFO Init\n";
	
	return ftStatus;
}

void AOMPresentation()
{
	int input = 0;

	
	char stepSize[]		= {0x00, 0x00, 0x10, 0x00};
	char rampRate[]		= {0x00, 0x01};
	
	//single tone
	createRamp(75e6, 75e6, stepSize, rampRate);
	cin >> input;

	//Slow Ramp (Pulse)
	//rampRate [1] = 0xF0;
	createRamp(100e6, 30e6, stepSize, rampRate);

	cin >> input;

	//Fast Ramp (Line)
	rampRate [1] = 0x01;
	createRamp(90e6, 75e6, stepSize, rampRate);

	cin >> input;


	
	//writeFileToRAM("../../Python/RAM_hex.txt", 124, 75e6, false);

	cin >> input;

	//Toggling Ramp
	rampRate [1] = 0x01;
	createRamp(70e6, 60e6, stepSize, rampRate);
	toggleRamp();

	for (int i=0; i < 1000; i++)
	{
		createRamp(90e6, 78e6, stepSize, rampRate);
		createRamp(72e6, 60e6, stepSize, rampRate);
	}
	cin >> input;
	toggleRamp();
}

void menuCW(int profile)
{
	float freq = 0.0;
	float amp = 0.0;

	cout << "\nEnter frequency: ";
	cin >> freq;

	cout << "\nEnter amplitude (between 0.0 and 1.0): ";
	cin >> amp;

	createCW(profile, amp, freq);
	goToProfile(profile);
}

void menuFM(int profile)
{

}

void menu ()
{
	int input = 0;
	int subinput = 0;
	do{
		cout << "Please select an output: \n";
		cout << "0: Help\n";
		cout << "1: Single Tone\n";
		cout << "2. FM Waveform (Tri Modulated)\n";
		cout << "3. FM Waveform (Sine Modulated Example)\n";
		cout << "4: Trigger\n";
		cout << "5: Sweep\n";
		cout << "Other: Exit\n";
		cin >> input;
		switch (input)
		{
		case 0:
			cout << "All frequency units are in Hz, time in seconds and amplitude in fractions of full output (0.0 to 1.0)\n";
			cout << "For more information please see technical document\n";
			cout << "Any questions please contact Dominic Oram at d.e.oram92@gmail.com\n";
			break;

		case 1:
			menuCW(0);
			break;
		case 3:
			//FM Sine Waveform Previously Created in Python
			writeFileToRAM("../../Python/RAM_hex - FM.txt", 124, 75e6, false);
			break;

		case 4:
			for (int i=0; i < 2; i++)
			{
				if (i == 0)
				{
					cout << "\nPlease enter first waveform: ";
				}else{
					cout << "\nPlease enter second waveform: ";					
				}

				menuCW(i);

			}
			toggleTrigger();

			break;
		case 5:
			float startFreq = 0.0;
			float endFreq = 0.0;
			float startDev = 0.0;
			float endDev = 0.0;
			float mod = 0.0;
			float time = 0.0;
			float endPower = 0.0;
			cout << "\nThis will sweep the output of the DDS between two triangular modulated waveforms, keeping a fixed modulation index.";
			cout << "\nStarting power of the waveform will be the full DDS power. The output will stay in the previously programmed state until a trigger is given, when the sweep will begin.\n";
			
			cout << "Enter starting frequency: ";
			cin >> startFreq;

			cout << "\nEnter end frequency: ";
			cin >> endFreq;

			cout << "\nEnter starting deviation: ";
			cin >> startDev;

			cout << "\nEnter end deviation: ";
			cin >> endDev;

			cout << "\nEnter modulation frequency: ";
			cin >> mod;

			cout << "\nEnter time period for full sweep: ";
			cin >> time;

			cout << "\nEnter final power (in fracttion of total power): ";
			cin >> endPower;

			writeSweep(startFreq, endFreq, startDev, endDev, mod, time, endPower);
			break;
		}
	}while(input != 5);
}


int main ()
{
	FT_STATUS open = 0;
	int tries = 2;
	int input = 1;

	do
	{		
		open |= setUpFT();
		open |= setUpForFPGA();
	
		setFPGADebugPrint(0);
	
		if (open != FT_OK)
		{
			cout << "Error in initializing device: " << open << "\n";
			closeFPGA();
			tries--;
			if (tries == -1) 
			{
				cout << "Boot failed, program terminated\n";
				Sleep(4000);
				return 1;
			}else{
				cout << "Retrying...\n";
				Sleep(1000);
			}
		
		}
	}while (open != FT_OK);
	
	cout << "Device initialized\n";

	//check if FPGA programmed, program if not
	cout << "Checking FPGA programmed\n";
	
	if (!isFPGAProg()){

		if (setUpMPSSE() != FT_OK) cout << "MPSSE not initialized\n";

		cout << "Programming FPGA...\n";

		if(writeSVF("../../VHDL/DDS.svf") != FT_OK)
		{
			closeFPGA();
			Sleep(2000);
			return 1;
		}
		Sleep(100);

	}

	//FPGA should now be programmed
	if (isFPGAProg()) cout << "FPGA programmed\n";
	else{
		closeFPGA();
		cout << "FPGA Programming failed\n";
		Sleep(2000);
		return 1;
	}

	cin >> input;

	closeFPGA();


	do{

		initFIFO();

		initDDS(ftHandleFIFO, 24.998389e6);

		menu();

		closeDDS();

	}while(input);

	closeFPGA();
}